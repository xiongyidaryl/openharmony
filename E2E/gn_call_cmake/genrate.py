import os
import re
import sys
from os.path import abspath

GN_DEFAULT = """# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/templates/metadata/module_info.gni")
import("//build/config/python.gni")
import("//build/config/clang/clang.gni")

template("ohos_build_thirdparty") {
    if (invoker.type == "shared") {
        _main_target_name = target_name
        _target_label = get_label_info(":${_main_target_name}", "label_with_toolchain")
        _subsystem_name = invoker.subsystem_name
        _part_name = invoker.part_name    
    }

    _deps = []
    if(defined(invoker.deps)) {
        _deps += invoker.deps
    }
    action(target_name) {
        forward_variables_from(invoker,
                                 [
                                   "sources",
                                   "outputs",
                                   "args",
                                   "script",
                                 ])
        deps = _deps
        if (invoker.type == "shared") {
            _install_module_info = {
              module_def = _target_label
              module_info_file =
                  rebase_path(get_label_info(_target_label, "target_out_dir"),
                              root_build_dir) +
                  "/${_main_target_name}_module_info.json"
              subsystem_name = _subsystem_name
              part_name = _part_name
              toolchain = current_toolchain
              toolchain_out_dir = rebase_path(root_out_dir, root_build_dir)
            }

            metadata = {
              install_modules = [ _install_module_info ]
            }
        }            
    }
    if (invoker.type == "shared") {
        generate_module_info("${_main_target_name}_info") {
            module_name = _main_target_name
            module_type = "lib"
            module_source_dir = "${target_out_dir}"
            install_enable = true
            module_install_images = [ "system" ]
        }   
    }        
}

config("@todo_thirdparty_name_config") {
    if (!defined(ldflags)) {
        ldflags = []
    }
    if (!defined(libs)) {
        libs = []
    }
    ldflags += [ "-L@todo_subsystem_name/@todo_part_name/lib" ]
    libs += [ "@todo_lib_name" ]
}

libtype = "@todo_lib_type"

ohos_build_thirdparty("@todo_thirdparty_lib_name") {
    type = libtype
    script = "build_thirdparty.sh"
    sources = [ "//third_party/@todo_thirdparty_name" ]
    outputs = [ "${target_out_dir}/@todo_thirdparty_lib_name" ]
    args = [
        rebase_path("//third_party/@todo_thirdparty_name",root_build_dir),
        rebase_path(target_out_dir,root_build_dir),
        rebase_path(root_build_dir,"//third_party/@todo_thirdparty_name")
    ]
    if (libtype == "shared") {
        subsystem_name = "@todo_subsystem_name"
        part_name = "@todo_part_name"    
    }
}

group("@todo_thirdparty_name") {
    deps = [
        ":@todo_thirdparty_lib_name",
    ]
}

"""
SH_DEFAULT = """#!/bin/sh
oh_build_root_dir=`pwd`
flag="-DCMAKE_TOOLCHAIN_FILE=../toolchain.cmake -DCMAKE_INSTALL_PREFIX=$oh_build_root_dir/@todo_subsystem_name/@todo_part_name @todo_define .."
cd @todo_path
mkdir thirdparty_build
cd thirdparty_build
cmake $flag
make -j
make install
cd -
mkdir -p $3/$2
cp thirdparty_lib/* $3/$2 -rf
"""

CMAKE_DEFAULT = """set(CMAKE_CROSSCOMPILING TRUE)
#set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(OHOS_SRC_HOME @todo_path)
set(OHOS_TOOLCHAIN ${OHOS_SRC_HOME}/prebuilts/clang/ohos/linux-x86_64/llvm/bin)
set(OHOS_SYSROOT ${OHOS_SRC_HOME}/out/@todo_platform/obj/third_party/musl)
set(CMAKE_C_COMPILER ${OHOS_TOOLCHAIN}/clang)
set(CMAKE_CXX_COMPILER ${OHOS_TOOLCHAIN}/clang++)
#set(CMAKE_TOOLCHAIN_PREFIX llvm-)
#SET_PROPERTY(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS TRUE)
set(CMAKE_C_FLAGS "--sysroot=${OHOS_SYSROOT} -Xclang -mllvm -Xclang -instcombine-lower-dbg-declare=0 --target=arm-linux-ohosmusl -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16 -mthumb -mfpu=neon")
set(CMAKE_CXX_FLAGS "--sysroot=${OHOS_SYSROOT} -fvisibility-inlines-hidden -Xclang -mllvm -Xclang -instcombine-lower-dbg-declare=0 --target=arm-linux-ohosmusl -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16 -mthumb -mfpu=neon")

set(MY_LINK_FLAGS "--target=arm-linux-ohosmusl --sysroot=${OHOS_SYSROOT}")
set(CMAKE_CXX_LINKER ${OHOS_TOOLCHAIN}/clang++)
set(CMAKE_C_LINKER ${OHOS_TOOLCHAIN}/clang)
set(CMAKE_C_LINK_EXECUTABLE "${CMAKE_C_LINKER} ${MY_LINK_FLAGS} <FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
set(CMAKE_CXX_LINK_EXECUTABLE "${CMAKE_CXX_LINKER} ${MY_LINK_FLAGS} <FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
set(CMAKE_FIND_USE_PACKAGE_ROOT_PATH FALSE)
set(CMAKE_FIND_USE_CMAKE_PATH FALSE)
set(CMAKE_FIND_USE_CMAKE_ENVIRONMENT_PATH FALSE)
#set(CMAKE_FIND_USE_SYSTEM_ENVIRONMENT_PATH FALSE)
set(CMAKE_FIND_USE_PACKAGE_REGISTRY FALSE)
#set(CMAKE_FIND_USE__PATH FALSE)
SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/thirdparty_lib)
SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/thirdparty_bin)
set(CMAKE_INSTALL_PREFIX ${OHOS_SRC_HOME}/out/@todo_platform/@todo_subsystem_name/@todo_part_name)
include_directories(${CMAKE_INSTALL_PREFIX}/include)
list(APPEND CMAKE_PREFIX_PATH  "${CMAKE_INSTALL_PREFIX}/include")
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_INSTALL_PREFIX}/lib")
"""

def build_gn():
    file = "thirdparty_config.json"
    path = os.getcwd()
    # print(path)
    file_path = os.path.join(path, file)
    pattern_parameter = '.*:"(.*)"'
    with open(file_path, 'r', encoding='utf-8') as json_file:
        for line in json_file:
            if line.replace(' ', '').startswith('"thirdparty_name"'):
                thirdparty_name = re.match(pattern_parameter, line).group(1).strip()
            elif line.replace(' ', '').startswith('"thirdparty_lib_name"'):
                thirdparty_lib = re.match(pattern_parameter, line).group(1).strip()
            elif line.replace(' ', '').startswith('"subsystem_name"'):
                subsystem = re.match(pattern_parameter, line).group(1).strip()
            elif line.replace(' ', '').startswith('"part_name"'):
                partname = re.match(pattern_parameter, line).group(1).strip()
            elif line.replace(' ', '').startswith('"defines"'):
                define = re.match(pattern_parameter, line).group(1).strip()
            elif line.replace(' ', '').startswith('"cpu"'):
                cpu = re.match(pattern_parameter, line).group(1).strip()
            elif line.replace(' ', '').startswith('"platform"'):
                platform = re.match(pattern_parameter, line).group(1).strip()
    lib_type = "static"
    if (thirdparty_lib.find(".so") >= 0):
        lib_type = "shared"
    lib_name = re.sub(r"lib", "", thirdparty_lib, count = 1)
    lib_name = re.sub(r"\.so.*", "", lib_name, count = 0)
    lib_name = re.sub(r"\.a.*", "", lib_name, count = 0)
    save_gn_file = os.path.join(path, "BUILD.gn")
    
    # if not os.path.exists(save_gn_file):
    print("==创建新的gn文件=")
    file = open(save_gn_file, 'w+')
    new_gn_str = GN_DEFAULT.replace("@todo_thirdparty_name",thirdparty_name).replace("@todo_thirdparty_lib_name",thirdparty_lib).replace("@todo_subsystem_name",subsystem).replace("@todo_part_name",partname).replace("@todo_lib_name",lib_name).replace("@todo_lib_type",lib_type)
    file.write(new_gn_str)
    print("---  OK  ---")
    file.close()

    save_sh_file = os.path.join(path, "build_thirdparty.sh")
    new_sh_str = SH_DEFAULT.replace('@todo_define', define).replace('@todo_path', path).replace("@todo_subsystem_name",subsystem).replace("@todo_part_name",partname)
    # if not os.path.exists(save_sh_file):
    print("==创建新的sh文件=")
    sh_file = open(save_sh_file, 'w')
    sh_file.write(new_sh_str)
    print("---  OK  ---")
    sh_file.close()
    os.chmod("build_thirdparty.sh", 0o777)

    save_cmake_file = os.path.join(path, "toolchain.cmake")
    new_cmake_str = CMAKE_DEFAULT.replace('@todo_platform', platform).replace('@todo_path', abspath(path+"/../..")).replace("@todo_subsystem_name",subsystem).replace("@todo_part_name",partname)
    print("==创建新的cmake文件=")
    cmake_file = open(save_cmake_file, 'w')
    cmake_file.write(new_cmake_str)
    cmake_file.close()
    if cpu == 'arm64':
        # print('here')
        new_data = ''
        with open(save_cmake_file, 'r', encoding='utf-8') as f:
            for line in f:
                if line.strip().startswith('set(CMAKE_C_FLAGS'):
                    line = 'set(CMAKE_C_FLAGS "--sysroot=${OHOS_SYSROOT} -Xclang -mllvm -Xclang -instcombine-lower-dbg-declare=0 --target=aarch64-linux-ohos")\n'
                elif line.strip().startswith('set(CMAKE_CXX_FLAGS'):
                    line = 'set(CMAKE_CXX_FLAGS "--sysroot=${OHOS_SYSROOT} -fvisibility-inlines-hidden -Xclang -mllvm -Xclang -instcombine-lower-dbg-declare=0 --target=aarch64-linux-ohos")'
                new_data += line
            f.close()
        with open(save_cmake_file, 'w', encoding='utf-8') as f:
            f.write(new_data)
            f.close()
    print("---  OK  ---")



if __name__ == '__main__':
    build_gn()


