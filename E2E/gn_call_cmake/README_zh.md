# GN脚本自动生成

## 背景

三方库生态中，有一部分三方库适配到OpenHarmony上是不需要修改原生库代码的，只需要解决编译构建问题就可以，对于这一类三方库，为了简化适配过程，我们可以使用脚本来完成这一任务。



## 实现方案

### 方案一

通过python解析日志

- 运行原生库构建命令
- 捕获生成的日志
- python依靠关键字来分析日志，获取构成BUILD.gn需要的元素
- 自动生成BUILD.gn脚本

方案一的优缺点：

优点：

- 可以直接融入OpenHarmony的构建体系

缺点：

- 需要通过特定的关键字来识别不同元素，针对不同的库，关键字可能不一样，需要重新适配，局限性较大
- 无法处理四方库的依赖



### 方案二

通过gn调用原生库构建脚本，因为不同的库构建脚本可能不一样，目前暂时支持cmake构建的脚本，以下以rk3568平台为例

- 为三方库配置OpenHarmony的工具链，编写toolchain.cmake文件

  ```
  set(CMAKE_CROSSCOMPILING TRUE)
  set(CMAKE_SYSTEM_NAME Linux)
  set(CMAKE_SYSTEM_PROCESSOR arm)
  set(OHOS_SRC_HOME /root/openharmony) #OpenHarmony源码目录
  set(OHOS_TOOLCHAIN ${OHOS_SRC_HOME}/prebuilts/clang/ohos/linux-x86_64/llvm/bin) #配置工具链
  set(OHOS_SYSROOT ${OHOS_SRC_HOME}/out/rk3568/obj/third_party/musl)
  set(CMAKE_C_COMPILER ${OHOS_TOOLCHAIN}/clang)
  set(CMAKE_CXX_COMPILER ${OHOS_TOOLCHAIN}/clang++)
  set(CMAKE_C_FLAGS "--sysroot=${OHOS_SYSROOT} -Xclang -mllvm -Xclang -instcombine-lower-dbg-declare=0 --target=aarch64-linux-ohos")
  set(CMAKE_CXX_FLAGS "--sysroot=${OHOS_SYSROOT} -fvisibility-inlines-hidden -Xclang -mllvm -Xclang -instcombine-lower-dbg-declare=0 --target=aarch64-linux-ohos")
  set(MY_LINK_FLAGS "--target=arm-linux-ohosmusl --sysroot=${OHOS_SYSROOT}")
  set(CMAKE_CXX_LINKER ${OHOS_TOOLCHAIN}/clang++)
  set(CMAKE_C_LINKER ${OHOS_TOOLCHAIN}/clang)
  set(CMAKE_C_LINK_EXECUTABLE "${CMAKE_C_LINKER} ${MY_LINK_FLAGS} <FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
  set(CMAKE_CXX_LINK_EXECUTABLE "${CMAKE_CXX_LINKER} ${MY_LINK_FLAGS} <FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
  SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/thirdparty_lib)
  SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/thirdparty_bin)
  set(CMAKE_INSTALL_PREFIX ${OHOS_SRC_HOME}/out/rk3568/xerces/xerces)
  include_directories(${CMAKE_INSTALL_PREFIX}/include)
  list(APPEND CMAKE_PREFIX_PATH  "${CMAKE_INSTALL_PREFIX}/include")
  list(APPEND CMAKE_PREFIX_PATH "${CMAKE_INSTALL_PREFIX}/lib")
  ```

- 编写BUILD.gn脚本，用来调用shell脚本

  ```
  import("//build/templates/metadata/module_info.gni")
  import("//build/config/python.gni")
  import("//build/config/clang/clang.gni")
  
  #自定义模板，该模板使用了action来调用shell脚本，然后将指定文件信息记录到系统中，方便系统后期拷贝打包到镜像中
  template("ohos_build_thirdparty") { 
      if (invoker.type == "shared") {
          _main_target_name = target_name
          _target_label = get_label_info(":${_main_target_name}", "label_with_toolchain")
          _subsystem_name = invoker.subsystem_name
          _part_name = invoker.part_name
      }
  
      _deps = []
      if(defined(invoker.deps)) {
          _deps += invoker.deps
      }
      action(target_name) {
          forward_variables_from(invoker,
                                   [
                                     "sources",
                                     "outputs",
                                     "args",
                                     "script",
                                   ])
          deps = _deps
          if (invoker.type == "shared") {
              _install_module_info = {
                module_def = _target_label
                module_info_file =
                    rebase_path(get_label_info(_target_label, "target_out_dir"),
                                root_build_dir) +
                    "/${_main_target_name}_module_info.json"
                subsystem_name = _subsystem_name
                part_name = _part_name
                toolchain = current_toolchain
                toolchain_out_dir = rebase_path(root_out_dir, root_build_dir)
              }
  
              metadata = {
                install_modules = [ _install_module_info ]
              }
          }
      }
      if (invoker.type == "shared") {
          generate_module_info("${_main_target_name}_info") {
              module_name = _main_target_name
              module_type = "lib"
              module_source_dir = "${target_out_dir}"
              install_enable = true
              module_install_images = [ "system" ]
          }
      }
  }
  
  #定义配置，其他库要引用这个库，必须依赖这个配置
  config("xerces-c_config") {
      if (!defined(ldflags)) {
          ldflags = []
      }
      if (!defined(libs)) {
          libs = []
      }
      ldflags += [ "-Lxxx/lib" ] #指定三方库所在路径
      libs += [ "xxxx" ] #指定三方库名
  }
  
  #声明是动态库还是静态库
  libtype = "shared" 
  
  ohos_build_thirdparty("xxxx.so") {
      type = libtype
      script = "build_thirdparty.sh"
      sources = [ "//third_party/xxxx" ]
      outputs = [ "${target_out_dir}/xxxx.so" ]
      args = [
          rebase_path("//third_party/xxx",root_build_dir),
          rebase_path(target_out_dir,root_build_dir),
          rebase_path(root_build_dir,"//third_party/xxx")
      ]
      if (libtype == "shared") {
          subsystem_name = "aaa"
          part_name = "aaa"
      }
  }
  
  group("xxxx") {
      deps = [
          ":xxxx.so",
      ]
  }
  
  
  ```

  

- 编写shell脚本，用来配置编译环境，执行构建命令

  ```
  #!/bin/sh
  flag="-DCMAKE_TOOLCHAIN_FILE=../toolchain.cmake  .."
  cd /root/openharmony/third_party/xxx #进入三方库xxx目录
  mkdir thirdparty_build
  cd thirdparty_build
  cmake $flag
  make -j
  make install
  cd -
  mkdir -p $3/$2
  cp thirdparty_lib/* $3/$2 -rf
  ```

  

- 通过上面的几个步骤，三个脚本每次针对不同的库改动量都非常小，所以我们使用python脚本来自动生成上述的几个文件，流程如下所示

![genrate](pic/genrate.png)

thirdparty_config.json的内容如下：

```
{
  "thirdparty_name":"三方库名", 
  "thirdparty_lib_name":"原生库生成的库的全名libxxx.so",
  "subsystem_name":"所属子系统",
  "part_name":"所属组件",
  "platform":"平台",
  "cpu":"arm64/arm",#64位或32位
  "defines":""
}
```



优点：

- 可扩展性较好，可以自己控制对四方库的依赖，也能融入构建体系



缺点：

- 在被其他库引用的时候，需要额外依赖config才能真正被使用，比常规的步骤多了一个步操作



### 细节

整个实现过程需要注意到的几个细节：

- 打包到镜像的关键是需要把库的信息嵌入到system_module_info.json中
- 为什么自定义模板？ 由于框架给定的拷贝函数ohos_copy需要指定和检查文件，无法做到先编译后检查，所以退一步使用action，先只进行编译，在编译完成后，再把相关信息写入json文件中，这样的步骤框架没有对应接口，所以自己来实现一个模板
- 为什么被别的库依赖需要config？由于我们编译库使用的是action，在框架中action不能被识别成文件，只是一个动作，通过分析编译过程文件（xxx.ninja）很容易看到，构建指定中，别的库依赖自定义模板时，并没有将库加入依赖，所以我们使用ldflag和libs来为库添加依赖，所以需要额外依赖config
- 区分动态库和静态库，静态库是不需要打包到镜像里面的



## 使用

```
#将thirdparty_config.json和genrate.py拷贝到三方库目录下
#打开thirdparty_config.json文件，完成基本信息的配置，然后执行脚本
python genrate.py
==创建新的gn文件=
---  OK  ---
==创建新的sh文件=
---  OK  ---
==创建新的cmake文件=
---  OK  ---
```

