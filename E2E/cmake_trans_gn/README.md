# E2E

#### 介绍
将cmake构建的的项目，转换为gn构建脚本

#### 使用说明

```
# 进入三方库，目录
cp 本目录的/parse_cmake_log ./
mkdir ohosbuild
cd ohosbuild
cmake ../ -L -DCMAKE_TOOLCHAIN_FILE=../parse_cmake_log/toolchain.cmake # -D cmake编译选项自行添加
make VERBOSE=1 > name_of_lib.log
cd ../parse_cmake_log
python generate_gn_file.py ../ohosbuild/name_of_lib.log
# 然后将会在三方库目录生成 BUILD.gn 文件
```
