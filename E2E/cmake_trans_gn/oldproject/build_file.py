import os
from os.path import relpath
from config import COPYRIGHT_DEFAULT

def build_gn_file(target, part_name):
    for key, value in target.items():
        if key == 'ohos_shared_library':
            for m, n in value.items():
                name = m.rstrip('.so')
                relative_source_list = list()
                relative_deps_list = list()
                relative_include_list = list()
                origin_path = n['path']
                file_path = '.'+n['path']
                if not (os.path.exists(file_path)):
                    os.makedirs(file_path)
                file = file_path+'/BUILD.gn'
                if os.path.exists(file):
                    config = ''
                    f = open(file, 'a+')
                    config = '\nconfig("' + name + '_shared_config") {'
                    if n['configs']['cflags_cc']:
                        config = config + '\n\n    cflags_cc = ['
                        for cflag in n['configs']['cflags_cc']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['configs']['cflags']:
                        config = config + '\n\n    cflags = ['
                        for cflag in n['configs']['cflags']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['include_dirs']:
                        config = config + '\n    include_dirs = ['
                        for include_dir in n['include_dirs']:
                            if include_dir == origin_path:
                                continue
                            else:
                                relative_include_list.append(
                                    '\n        "' + relpath(include_dir, origin_path).replace("\\", "/") + '",')
                        for relative_include in relative_include_list:
                            config = config + relative_include
                        config = config + '\n    ]\n}\n\n'
                    else:
                        config = config + '}\n\n'
                else:
                    f = open(file, 'w+')
                    f.write(COPYRIGHT_DEFAULT)
                    config = '\nconfig("'+name+'_shared_config") {'
                    if n['configs']['cflags_cc']:
                        config = config + '\n\n    cflags_cc = ['
                        for cflag in n['configs']['cflags_cc']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['configs']['cflags']:
                        config = config + '\n\n    cflags = ['
                        for cflag in n['configs']['cflags']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['include_dirs']:
                        config = config + '\n    include_dirs = ['
                        for include_dir in n['include_dirs']:
                            if include_dir == origin_path:
                                continue
                            else:
                                relative_include_list.append(
                                    '\n        "' + relpath(include_dir, origin_path).replace("\\", "/") + '",')
                        for relative_include in relative_include_list:
                            config = config + relative_include
                        config = config + '\n    ]\n}\n\n'
                    else:
                        config = config + '}\n\n'
                title = 'ohos_shared_library("'+name+'") {'
                new_string = config + title
                if n['sources']:
                    new_string = new_string + '\n     \n    sources = ['
                    for source in n['sources']:
                        relative_source_list.append(
                            '\n        "' + relpath(source, origin_path).replace("\\", "/") + '",')
                    for relative_source in relative_source_list:
                        new_string = new_string + relative_source
                    new_string = new_string + '\n    ]\n\n    configs = [ \n        ":'+name+'_shared_config"'
                else:
                    new_string = new_string + '\n    configs = [ \n        ":'+name+'_shared_config"'
                if n['deps']:
                    new_string = new_string + '\n     ]\n\n    deps = ['
                    for dep in n['deps']:
                        if dep == m:
                            continue
                        elif dep.endswith('.so'):
                            dep_name = dep.rstrip('.so')
                            try:
                                relative_deps_list.append(
                                    '\n        "' + relpath(target['ohos_shared_library'][dep]['path'],
                                                            origin_path).replace("\\", "/").lstrip('.') + ':' + dep_name + '",')
                            except:
                                relative_deps_list.append('\n        "outside:' + dep_name + '",')
                        elif dep.endswith('.a'):
                            dep_name = dep.rstrip('.a')
                            try:
                                relative_deps_list.append(
                                    '\n        "' + relpath(target['ohos_static_library'][dep]['path'],
                                                            origin_path).replace("\\", "/").lstrip('.') + ':' + dep_name + '",')
                            except:
                                relative_deps_list.append('\n        "outside:' + dep_name + '",')
                    for relative_deps in relative_deps_list:
                        new_string = new_string + relative_deps
                    new_string = new_string + '\n    ]\n\n    part_name = "'+part_name+'"'
                else:
                    new_string = new_string + '\n    ]\n\n    part_name = "'+part_name+'"'
                end = '\n}\n\n'
                new_string = new_string + end
                f.write(new_string)
                f.close()
        elif key == "ohos_executable":
            for m, n in value.items():
                name = m
                relative_source_list = list()
                relative_deps_list = list()
                relative_include_list = list()
                origin_path = n['path']
                file_path = '.' + n['path']
                if not (os.path.exists(file_path)):
                    os.makedirs(file_path)
                file = file_path + '/BUILD.gn'
                if os.path.exists(file):
                    config = ''
                    f = open(file, 'a+')
                    config = '\nconfig("' + name + '_config") {'
                    if n['configs']['cflags_cc']:
                        config = config + '\n\n    cflags_cc = ['
                        for cflag in n['configs']['cflags_cc']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['configs']['cflags']:
                        config = config + '\n\n    cflags = ['
                        for cflag in n['configs']['cflags']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['include_dirs']:
                        config = config + '\n    include_dirs = ['
                        for include_dir in n['include_dirs']:
                            if include_dir == origin_path:
                                continue
                            else:
                                relative_include_list.append(
                                    '\n        "' + relpath(include_dir, origin_path).replace("\\", "/") + '",')
                        for relative_include in relative_include_list:
                            config = config + relative_include
                        config = config + '\n    ]\n}\n\n'
                    else:
                        config = config + '}\n\n'
                else:
                    f = open(file, 'w+')
                    f.write(COPYRIGHT_DEFAULT)
                    config = '\nconfig("' + name + '_config") {'
                    if n['configs']['cflags_cc']:
                        config = config + '\n\n    cflags_cc = ['
                        for cflag in n['configs']['cflags_cc']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['configs']['cflags']:
                        config = config + '\n\n    cflags = ['
                        for cflag in n['configs']['cflags']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['include_dirs']:
                        config = config + '\n    include_dirs = ['
                        for include_dir in n['include_dirs']:
                            if include_dir == origin_path:
                                continue
                            else:
                                relative_include_list.append(
                                    '\n        "' + relpath(include_dir, origin_path).replace("\\", "/") + '",')
                        for relative_include in relative_include_list:
                            config = config + relative_include
                        config = config + '\n    ]\n}\n\n'
                    else:
                        config = config + '}\n\n'
                title = 'ohos_executable("' + name + '") {'
                new_string = config + title
                if n['sources']:
                    new_string = new_string + '\n     \n    sources = ['
                    for source in n['sources']:
                        relative_source_list.append(
                            '\n        "' + relpath(source, origin_path).replace("\\", "/") + '",')
                    for relative_source in relative_source_list:
                        new_string = new_string + relative_source
                    new_string = new_string + '\n    ]\n\n    configs = [ \n        ":' + name + '_config"'
                else:
                    new_string = new_string + '\n    configs = [ \n        ":' + name + '_config"'
                if n['deps']:
                    new_string = new_string + '\n     ]\n\n    deps = ['
                    for dep in n['deps']:
                        if dep == m:
                            continue
                        elif dep.endswith('.so'):
                            dep_name = dep.rstrip('.so')
                            try:
                                relative_deps_list.append(
                                    '\n        "' + relpath(target['ohos_shared_library'][dep]['path'],
                                                            origin_path).replace("\\", "/").lstrip('.') + ':' + dep_name + '",')
                            except:
                                relative_deps_list.append('\n        "outside:' + dep_name + '",')
                        elif dep.endswith('.a'):
                            dep_name = dep.rstrip('.a')
                            try:
                                relative_deps_list.append(
                                    '\n        "' + relpath(target['ohos_static_library'][dep]['path'],
                                                            origin_path).replace("\\", "/").lstrip('.') + ':' + dep_name + '",')
                            except:
                                relative_deps_list.append('\n        "outside:' + dep_name + '",')
                    for relative_deps in relative_deps_list:
                        # print('relative_deps:'+relative_deps)
                        new_string = new_string + relative_deps
                new_string = new_string + '\n    ]\n\n    part_name = "' + part_name + '"'
                end = '\n}\n\n'
                new_string = new_string + end
                f.write(new_string)
                f.close()
        elif key == 'ohos_static_library':
            for m, n in value.items():
                name = m.rstrip('.a')
                relative_source_list = list()
                relative_deps_list = list()
                relative_include_list = list()
                origin_path = n['path']
                file_path = '.' + n['path']
                if not (os.path.exists(file_path)):
                    os.makedirs(file_path)
                file = file_path + '/BUILD.gn'
                if os.path.exists(file):
                    config = ''
                    f = open(file, 'a+')
                    config = '\nconfig("' + name + '_static_config") {'
                    if n['configs']['cflags_cc']:
                        config = config + '\n\n    cflags_cc = ['
                        for cflag in n['configs']['cflags_cc']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['configs']['cflags']:
                        config = config + '\n\n    cflags = ['
                        for cflag in n['configs']['cflags']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['include_dirs']:
                        config = config + '\n    include_dirs = ['
                        for include_dir in n['include_dirs']:
                            if include_dir == origin_path:
                                continue
                            else:
                                relative_include_list.append(
                                    '\n        "' + relpath(include_dir, origin_path).replace("\\", "/") + '",')
                        for relative_include in relative_include_list:
                            config = config + relative_include
                        config = config + '\n    ]\n}\n\n'
                    else:
                        config = config + '}\n\n'
                else:
                    f = open(file, 'w+')
                    f.write(COPYRIGHT_DEFAULT)
                    config = '\nconfig("' + name + '_static_config") {'
                    if n['configs']['cflags_cc']:
                        config = config + '\n\n    cflags_cc = ['
                        for cflag in n['configs']['cflags_cc']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['configs']['cflags']:
                        config = config + '\n\n    cflags = ['
                        for cflag in n['configs']['cflags']:
                            cflag = cflag.replace('"', '\'')
                            config = config + '\n        "' + cflag + '",'
                        config = config + '\n    ]\n'
                    if n['include_dirs']:
                        config = config + '\n    include_dirs = ['
                        for include_dir in n['include_dirs']:
                            if include_dir == origin_path:
                                continue
                            else:
                                relative_include_list.append(
                                    '\n        "' + relpath(include_dir, origin_path).replace("\\", "/") + '",')
                        for relative_include in relative_include_list:
                            config = config + relative_include
                        config = config + '\n    ]\n}\n\n'
                    else:
                        config = config + '}\n\n'
                title = 'ohos_static_library("' + name + '") {'
                new_string = config + title
                if n['sources']:
                    new_string = new_string + '\n     \n    sources = ['
                    for source in n['sources']:
                        relative_source_list.append(
                            '\n        "' + relpath(source, origin_path).replace("\\", "/") + '",')
                    for relative_source in relative_source_list:
                        new_string = new_string + relative_source
                    new_string = new_string + '\n    ]\n\n    configs = [ \n        ":' + name + '_static_config"'
                else:
                    new_string = new_string + '\n    configs = [ \n        ":' + name + '_static_config"'
                new_string = new_string + '\n    ]\n\n    part_name = "' + part_name + '"'
                end = '\n}\n\n'
                new_string = new_string + end
                f.write(new_string)
                f.close()


if __name__ == "__main__":
    build_gn_file()