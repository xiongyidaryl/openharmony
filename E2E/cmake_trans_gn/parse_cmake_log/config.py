
'''
    ohos clang cross compiler
'''
COMPILER_CC = "prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang "
COMPILER_CXX = "prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang++ "
COMPILER_AR = "prebuilts/clang/ohos/linux-x86_64/llvm/bin/llvm-ar "

'''
    linux gcc compiler
'''
# COMPILER_CC = "/usr/bin/cc "
# COMPILER_CXX = "/usr/bin/c++ "
# COMPILER_AR = "/usr/bin/ar "

'''分割各个target的日志段'''
SLICE_TARET = '%] Built target '

'''分割每个target构建时编译段日志和链接段日志'''
LINK_TARGET = "%] Linking "

'''需要忽略的编译选项'''
COMLILE_BLACK_OPTION0 = ("-MD", "-Xclang", "-mllvm", "-Xclang", "-instcombine-lower-dbg-declare=0", "--target=aarch64-linux-ohos")
'''需要忽略的编译选项以及其后面的一个也要忽略'''
COMLILE_BLACK_OPTION1 = ("-MT", "-MF", "-o")

'''工作目录'''
WorkingDir = ""

'''目标类型 shared static executable'''
TARGET_TYPE = ("shared", "static", "executable")

LINK_BLACK_OPTION0 = ("-shared", "-Xclang", "-mllvm", "-Xclang", "-instcombine-lower-dbg-declare=0", "--target=aarch64-linux-ohos")
LINK_BLACK_OPTION1 = ("-o")

OHOS_COPYRIGHT = '''# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")

'''

OHOS_CONFIG_TEMPLATE = '''
config("{0}_config") {{
  include_dirs = {1}
  
  cflags = {2}
  
  cflags_cc = {3}

  defines = {4}

}}
'''

OHOS_SHARED_TARGET_TEMPLATE = '''
ohos_shared_library("{0}") {{
  sources = {1}

  deps = {2}

  public_configs = [ ":{0}_config" ]

  part_name = "{3}"
}}
'''

OHOS_STATIC_TARGET_TEMPLATE = '''
ohos_static_library("{0}") {{
  sources = {1}

  deps = {2}

  public_configs = [ ":{0}_config" ]

  part_name = "{3}"
}}
'''

OHOS_EXE_TARGET_TEMPLATE = '''
ohos_executable("{0}") {{
  sources = {1}

  deps = {2}

  public_configs = [ ":{0}_config" ]

  part_name = "{3}"
}}
'''
