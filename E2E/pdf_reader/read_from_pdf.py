
import pdfplumber

def read_from_pdf(file_path):
    """
    读取pdf文件
    """
    import pdfplumber

    with pdfplumber.open(file_path) as pdf:
        # dict = pdf.metadata
        # pages_info = pdf.pages
        #  print(dict)
        #  print(pages_info)
        for page in pdf.pages:
            # text in one page
            text_content = page.extract_text()
            print(text_content)
            # tables content in one page
            tables = page.extract_tables()
            for table in tables:
                print(table)

            print('---------- 分页分隔 ----------')


if __name__ == '__main__':
    read_from_pdf('data/opensl-es-1-1-quick-reference.pdf')