import datetime
import os

import openpyxl
from bs4 import BeautifulSoup
from config import OUTPUT_DIRS_DEFAULT
from spiders import spider


def __get_openGL_ES_functions(flag, response):
    apis = []
    resp = response.text
    soup = BeautifulSoup(resp, "html.parser")
    div = soup.find('div', id='navwrap')
    # print(div)
    ul_list = div.find_all('ul', class_='Level3')
    for ul in ul_list:
        # print(ul.text)
        li_list = ul.find_all('li')
        for li in li_list:
            apis.append(li.text)
    return apis



def get_openGL_ES_diff(classifylist):
    task_list = []
    for url, flag in classifylist:
        task = (url, flag)
        task_list.append(task)
    result = spider(10, task_list)
    api_dict = {}
    for flag, response in result:
        if response.status_code == 200:
            # print(response)
            api_dict[flag] = __get_openGL_ES_functions(flag, response)
            build_file(flag, api_dict[flag], 'xlsx')
        else:
            print("ERR:Failed to connect html")
    # print(api_dict)
    set_GL = set(api_dict['openGL'])
    set_GL_ES = set(api_dict['openGL_ES'])
    set_diff = set_GL ^ set_GL_ES
    print(list(set_diff))
    build_file('diff', list(set_diff), 'xlsx')


def build_file(flag, apis, type, save_file=None):
    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)
    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, flag + "_api." + type)
    if type == 'xlsx':
        wb = openpyxl.Workbook()
        wb.remove(wb.active)
        sheet = wb.create_sheet(flag)
        sheet.cell(1, 1, "symbol")
        sheet.cell(1, 2, "type")
        sheet.cell(1, 3, "header")
        sheet.cell(1, 4, "modules/class")
        sheet.cell(1, 5, "status")
        sheet.cell(1, 6, "symbol_on_openharmony")
        sheet.cell(1, 7, "remarks")
        row = 2
        for api in apis:
            sheet.cell(row, 1, api)
            sheet.cell(row, 2, 'FUNCTION')
            sheet.cell(row, 4, 'openGLES')
            sheet.cell(row, 5, 'WARNING')
            row += 1
        wb.save(save_file)
    elif type == 'txt':
        file = open(save_file, 'w')
        for api in apis:
            file.writelines(api+'\n')
    return

if __name__ == '__main__':
    # classifylist = [('https://registry.khronos.org/OpenGL-Refpages/gl4/', 'openGL'),('https://registry.khronos.org/OpenGL-Refpages/es3/','openGLES')]
    classifylist = [('https://registry.khronos.org/OpenGL-Refpages/gl4/html/indexflat.php', 'openGL'),
                    ('https://registry.khronos.org/OpenGL-Refpages/es3/html/indexflat.php', 'openGL_ES')]

    get_openGL_ES_diff(classifylist)