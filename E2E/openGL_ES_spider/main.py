# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

# Press the green button in the gutter to run the script.
from get_openGL_ES import get_openGL_ES

if __name__ == '__main__':
    classifylist = [('https://registry.khronos.org/OpenGL-Refpages/gl4/html/indexflat.php', 'openGL'),
                    ('https://registry.khronos.org/OpenGL-Refpages/es3/html/indexflat.php', 'openGL_ES')]

    get_openGL_ES(classifylist)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
