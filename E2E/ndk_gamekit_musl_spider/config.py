#!/usr/bin/env python
# coding = UTF-8

OUTPUT_DIRS_DEFAULT = "./out"
DEFINE_FUNCTIONS = "Function"
DEFINE_TYPEDEFS = "Typedef"
DEFINE_STRUCTS = "Struct"
DEFINE_ENUMERATIONS = "Enumeration"
DEFINE_VARIABLES = "Variable"
DEFINE_MACROS = "Macro"
GAMEKIT_URL = "https://developer.android.google.cn/games/agdk/libraries-overview"