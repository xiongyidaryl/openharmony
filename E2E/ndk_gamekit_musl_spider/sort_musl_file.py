import numpy as np
import pandas as pd


def sort_musl_file(musl_file, save_file_dir=None):
    save_file_dir = "out/musl.xlsx"
    df = pd.ExcelFile(musl_file)
    sheet_names = df.sheet_names
    df1 = pd.read_excel(musl_file, sheet_name=0)
    for i in range(1, len(sheet_names)):
        df2 = pd.read_excel(musl_file, sheet_name=i)
        df1 = pd.concat([df1, df2], ignore_index=True)
    # print(df1['type'])
    df1['type'].replace('\xa0', 'FUNCTION')
    df1 = pd.DataFrame(df1)
    # print(df1)
    df_afterMerge = df1.copy().drop_duplicates(subset=['symbol', 'type', 'header', 'status'], inplace=False)
    df_missing = df_afterMerge.loc[df_afterMerge['status'] == 'missing']
    # print(df_missing)
    writer = pd.ExcelWriter(save_file_dir)
    df_missing.to_excel(writer, index=False)
    writer.save()
    writer.close()



if __name__ == "__main__":
    save_file = 'data/musl_compatibility.xlsx'
    sort_musl_file(save_file)
