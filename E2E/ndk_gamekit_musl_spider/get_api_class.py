from bs4 import BeautifulSoup
from spiders import spider


def get_class_details(response):
    data = []
    web = "https://developer.android.google.cn"
    resp = response.text
    soup = BeautifulSoup(resp, "html.parser")
    modules_table = soup.find('table', class_='details responsive')
    tr_list = modules_table.find_all('tr')
    # print(tr_list)
    for tr in tr_list:
        td = tr.find('td')
        try:
            a = td.find('a')
            url = web + a.get('href')
            flag = a.text
            type = "modules"
            classify = (flag, url, type)
            # print(classify)
            # print(type(classify))
            data.append(classify)
        except:
            continue
    classes_table = soup.find('table', class_='nested-classes responsive')
    tr_list = classes_table.find_all('tr')
    # print(tr_list)
    for tr in tr_list:
        td = tr.find('td')
        try:
            a = td.find('a')
            url = web + a.get('href')
            flag = a.text
            type = "classes"
            classify = (flag, url, type)
            # print(classify)
            # print(type(classify))
            data.append(classify)
        except:
            continue
    return data

def get_api_in_homepage():
    url = "https://developer.android.google.cn/ndk/reference"
    tasks = list()
    api_dict = dict()
    classify = list()
    flag = ['ndk']
    task = (url, flag)
    tasks.append(task)
    ret = spider(20, tasks)
    for flag, response in ret:
        if response.status_code == 200:
            # print(response.text)
            classify = get_class_details(response)
    api_dict['ndk'] = classify
    print(api_dict)
    return api_dict

if __name__ == "__main__":
    get_api_in_homepage()
    # language_list = [('C', 'https://gitee.com/explore/all?lang=C&order=starred'),
    #                  ('C++', 'https://gitee.com/explore/all?lang=cpp&order=starred')]
    # get_page_in_homepage(language_list)