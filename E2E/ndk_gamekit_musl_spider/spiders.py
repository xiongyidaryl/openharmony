import time
from concurrent.futures import ThreadPoolExecutor
import requests
import datetime
from requests.adapters import HTTPAdapter


def __get_time():
    current_time = datetime.datetime.now().strftime('%Y-%m-%d %X')
    return current_time


def time_me(fn):
    def _wrapper(*args, **kwargs):
        start = datetime.datetime.now()	 # 获取当前时间
        ret = fn(*args, **kwargs)
        end = datetime.datetime.now()
        print(f"[{__get_time()}] --->function:{fn.__name__} {args} {kwargs} cost {(end-start).seconds} seconds")
        return ret
    return _wrapper

@time_me
def get_android_api_request(url, flag):
    requests.adapters.DEFAULT_RETRIES = 10
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36',
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'X-Requested-With': 'XMLHttpRequest',
    }
    try:
        resp = requests.get(url, headers=headers)
    except requests.exceptions.RequestException as e:
        print(f"spider {url} {headers} Exception")
        return None
    return flag, resp

def __action(url: str, flag: dict):
    response = get_android_api_request(url, flag)
    # time.sleep(1)
    return response

def spider(max_workers: int, tasks: list):
    result = list()
    futures = list()
    with ThreadPoolExecutor(max_workers) as pool:
        for task in tasks:
            url = task[0]
            flag = task[1]
            futures.append(pool.submit(__action, url, flag))
    for future in futures:
        result.append(future.result())
    return result

def __main():
    url = "https://gitee.com/explore/all?order=starred"
    tasks = list()
    for page in range(1):
        # param = dict()
        # param["access_token"] = "1b0038e8549006c6f55fc654cf43f6de"
        # param["page"] = page
        # param["per_page"] = 15
        flag = ['全部项目']
        # param["type"] = "all"
        task = (url, flag)
        tasks.append(task)
    ret = spider(20, tasks)
    print(ret)

if __name__ == "__main__":
    __main()