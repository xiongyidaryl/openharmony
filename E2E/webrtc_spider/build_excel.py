import openpyxl as openpyxl
import datetime
import os


OUTPUT_DIRS_DEFAULT = "./out"

def get_github_deps(deps_list, absl_deps_list, save_file=None):
    print('111')
    print(os.getcwd())
    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)
    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, "github_deps.xlsx")
    wb = openpyxl.Workbook()
    wb.remove(wb.active)
    sheet = wb.create_sheet('sheet1')
    sheet.cell(1, 1, "deps")
    sheet.cell(1, 2, "absl_deps")
    row = 2
    for deps in deps_list:
        sheet.cell(row, 1, deps)
        row += 1
    row = 2
    for absl_deps in absl_deps_list:
        sheet.cell(row, 2, absl_deps)
        row += 1
    wb.save(save_file)
    return




