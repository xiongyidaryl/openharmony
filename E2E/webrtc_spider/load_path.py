#!/usr/bin/env python
# coding = UTF-8
import os.path
import re
from os.path import relpath
from build_excel import get_github_deps

def get_path_datas(path):
    root_dir = path
    file_list = []
    dir_list = []
    deps_list = []
    absl_deps_list = []
    real_deps_list = []
    dict = {}
    original_path = os.getcwd()
    for root, dirs, files in os.walk(root_dir):
        # for dir in dirs:  # 遍历目录里的所有文件夹
        #     print(os.path.join(root, dir), " --- dir")
        for file in files:  # 遍历目录里的所有文件
            if file == 'BUILD.gn':
                file_list.append(os.path.join(root, file))
                dir_list.append(root)
                print(os.path.join(root, file), " --- file")
                print(root, " --- root")
                dict[root] = os.path.join(root, file)
    # print(file_list)
    # print(dir_list)
    # print(dict)
    for x, y in dict.items():
        print(y)
        print(x)
        os.chdir(x)
        last_end = 0
        contents = open(y, 'r')
        all_the_text = contents.readlines()
        for i in range(len(all_the_text)):
            if all_the_text[i].find('  ]') !=-1 or all_the_text[i].find('    ]') != -1:
                for j in range(last_end+1, i):
                    if all_the_text[j].find('absl_deps = [') != -1 and all_the_text[j].find(']') == -1:
                        for k in range(j, i):
                            deps_pattern = re.compile(r"\s+\"(.*)\",")
                            try:
                                deps = re.match(deps_pattern, all_the_text[k]).group(1)
                                # print("no single line" + deps + " "+ str(k))
                                if deps not in absl_deps_list:
                                    absl_deps_list.append(deps)
                            except:
                                continue
                    elif all_the_text[j].find('absl_deps = [') != -1 and all_the_text[j].find(']') != -1:
                        # print("unique line")
                        single_deps_pattern = re.compile(r'"(.*)"')
                        try:
                            deps = re.search(single_deps_pattern, all_the_text[j]).group(1)
                            # print("unique line" + deps)
                            if deps not in absl_deps_list:
                                absl_deps_list.append(deps)
                        except:
                            continue
                    elif all_the_text[j].find('deps = [') != -1 and all_the_text[j].find(']') == -1:
                        for k in range(j, i):
                            deps_pattern = re.compile(r"\s+\"(.*)\",")
                            try:
                                deps = re.match(deps_pattern, all_the_text[k]).group(1)
                                # print("no single line" + deps + " "+ str(k))
                                if deps.find('..:') != -1:
                                    # print('yes')
                                    deps = deps.replace('..', '../')
                                # print(deps)
                                abs_path = os.path.abspath(deps)
                                rel_path = relpath(abs_path, original_path)
                                # invalid_pattern = re.compile(r".*\\+.*\\?:..:.*")
                                # invalid_str = re.match(invalid_pattern, rel_path).group(1)
                                # print("invalid  str " + invalid_str)
                                print(abs_path)
                                print(rel_path)
                                if rel_path not in deps_list and abs_path not in real_deps_list:
                                    deps_list.append(rel_path)
                                    real_deps_list.append(abs_path)
                            except:
                                continue
                    elif all_the_text[j].find('deps = [') != -1 and all_the_text[j].find(']') == -1:
                        # print("unique line")
                        single_deps_pattern = re.compile(r'"(.*)"')
                        try:
                            deps = re.search(single_deps_pattern, all_the_text[j]).group(1)
                            if deps.find('..:') != -1:
                                # print('yes')
                                deps = deps.replace('..', '../')
                            # print(deps)
                            print("unique line" + deps)
                            abs_path = os.path.abspath(deps)
                            rel_path = relpath(abs_path, original_path)
                            print(abs_path)
                            print(rel_path)
                            if rel_path not in deps_list and abs_path not in real_deps_list:
                                deps_list.append(rel_path)
                                real_deps_list.append(abs_path)
                        except:
                            continue
                last_end = i
    print(deps_list)
    print(absl_deps_list)
    os.chdir(original_path)
    get_github_deps(deps_list, absl_deps_list)
    return


if __name__ == "__main__":
    get_repos_all()