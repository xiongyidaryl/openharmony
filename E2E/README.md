# E2E

## **三方库价值识别**



## **风险识别**

1. 目前已经爬取的 Android NDK, Android Games, musl libc99, libc11, posix 的API,作为黑名单，用于三方库的风险比对.(其中libc99, libc11, posix 超出 musl 范围的API 作为黑名单)
2. 黑名单后续还会增加 OpenSL ES 与 OpenSL的差集, OpenGL ES 和 OpenGL的差集.以及Android bionic C库 与 musl 的差集.
3. 黑名单用于识别的三方库中对平台依赖的部分，因此此很名单会随着项目经验的增长不停扩大.
4. 目前工作重心：将已经搜集的黑名单和识别出的三方库进行符号对比，判定是否包含黑名单中的符号

## **编译脚本生成工具**

1. GN 脚本直接调用cmake

   动态库，静态库均以分别实现。

2. 使用 GN 实现编译过程

   编译日志已经分析完成，方案可行。脚本工具实现中。

## **NDK框架生成工具**