import os

import openpyxl
import config
from black_list import BlackList
from find_api_in_source import find_api_in_source
from get_libs_from_dir import get_libs_from_dir
from tasks_pool import Tasks


def __task_func(param):
    """
    description: scan source files used warning apis and return dict saved details
    """
    apis = param[0]
    lib = param[1]
    lib_info = param[2]
    apis_dict = param[3]
    files = lib_info['sources']
    total_line = 0
    find_file_apis_info = {}
    lib_ret = {}

    for file in files:
        find_apis_dict, file_line = find_api_in_source(file, apis, apis_dict)
        if find_apis_dict:
            find_file_apis_info[file] = find_apis_dict
        total_line = total_line + file_line
    lib_ret['lib_name'] = lib
    lib_ret['find_file_apis'] = find_file_apis_info
    lib_ret['total_line'] = total_line
    return lib_ret

def relevant_func_analyzer(software_root, blackList, file_name, save_path, root_name):
    """
    description: return blackList/greyList excel file
    """
    total_line = 0
    libs = get_libs_from_dir(software_root, True)
    tasks = Tasks(target=__task_func)
    lib_names = libs.keys()
    for lib_name in lib_names:
        lib_info = libs[lib_name]
        tasks.dispatch_task((blackList.apis_list, lib_name, lib_info, blackList.apis_dict))
    tasks.wait_tasks_end()
    libs_deps = tasks.get_result()
    output_file = save_path + root_name +'_risk_assessment.xlsx'
    if not (os.path.exists(output_file)):
        wb = openpyxl.Workbook()
        wb.remove(wb['Sheet'])
        sheet = wb.create_sheet('full_result')
        sheet.cell(1, 1, 'thirdParty_name')
        sheet.cell(1, 2, 'symbol')
        sheet.cell(1, 3, 'type')
        sheet.cell(1, 4, 'header')
        sheet.cell(1, 5, 'module/class')
        sheet.cell(1, 6, 'status')
        sheet.cell(1, 7, 'symbol_on_openharmony')
        sheet.cell(1, 8, 'remarks')
        sheet.cell(1, 9, 'file_path')
        sheet.cell(1, 10, 'line')
        sheet1 = wb.create_sheet('conclusion')
        sheet1.cell(1, 1, '三方库名称')
        sheet1.cell(1, 2, '风险分析')
        sheet1.cell(1, 3, '风险项(个数)')
        sheet1.cell(1, 4, '三方库团队建议')
        sheet1.cell(1, 5, '预估工作量')
        sheet1.cell(1, 6, '原生构建工具')
        sheet1.cell(1, 7, '行数')
        wb.save(output_file)
        wb.close()
    wb1 = openpyxl.load_workbook(output_file)
    sheet = wb1.get_sheet_by_name('full_result')
    for lib in libs_deps:
        total_line = total_line + lib['total_line']
        if lib['find_file_apis']:
            for file_path, file_info in lib['find_file_apis'].items():
                for line, items in file_info.items():
                    for item in items:
                        for saved_info in item:
                            target_excel = saved_info[0]
                            api_info = saved_info[1]
                            max_row = sheet.max_row
                            sheet.cell(max_row + 1, 1, file_name)
                            sheet.cell(max_row + 1, 2, api_info['symbol'])
                            sheet.cell(max_row + 1, 3, api_info['type'])
                            sheet.cell(max_row + 1, 4, api_info['header'])
                            sheet.cell(max_row + 1, 5, api_info['modules/class'])
                            sheet.cell(max_row + 1, 6, api_info['status'])
                            sheet.cell(max_row + 1, 7, api_info['symbol_on_openharmony'])
                            sheet.cell(max_row + 1, 8, api_info['remarks'])
                            sheet.cell(max_row + 1, 9, file_path[len(config.THIRD_LIB_ROOT) + 1:])
                            sheet.cell(max_row + 1, 10, line)
                            # sheet.cell(max_row + 1, 11, lib['total_line'])
        else:
            continue
    sheet1 = wb1.get_sheet_by_name('conclusion')
    max_row = sheet1.max_row
    sheet1.cell(max_row + 1, 1, file_name)
    sheet1.cell(max_row + 1, 7, total_line)
    wb1.save(output_file)
    wb1.close()



if __name__ == '__main__':
    black_list_test = BlackList('./data/blacklist')
    grey_list_test = BlackList('./data/greylist')
    relevant_func_analyzer('./data/test', black_list_test, 'black_list', 'test', './out/')
    relevant_func_analyzer('./data/test', grey_list_test, 'grey_list', 'test', './out/')
