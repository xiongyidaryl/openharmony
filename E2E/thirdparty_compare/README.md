## 概述
项目用于评估三方库在移植到，OpenHarmony上的风险，主要是不同系统上symbol不兼容问题。

## 使用方法
clone 本项目，进入本目录， -f 输入存放三方库文件夹的上层路径 -b 可选，输入黑名单excel表格存放路径(默认./back_list) -g 可选，输入灰名单excel表格存放路径(默认./grey_list) -d 可选，文件输出路径(默认./out)

```
python main.py -f thirdPartLibDir -b blackListDir(optional) -g greyListDir(optional) -d outputPath(optional) 
```
## 注意事项
注意输入的三方库文件夹路径内部分含有注释的文件代码会修改并删除注释代码，建议运行前备份一份原文件。

## 扩展
用户自定义补充的黑名单表格 openharmony\E2E\thirdparty_compare\black_list\ 可以补充到这个文件夹中，表格目前仅支持.xlsx, .xls格式，表格内容需按默认格式填写。
