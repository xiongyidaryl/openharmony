import openpyxl
from openpyxl.utils import get_column_letter

def ReadSheet(wb, sheetName = 'risk_summary', backUpData = None):
    try:
        sht = wb[sheetName]
    except Exception as err:
        print('open {0} sheet failed!'.format(sheetName))
        if backUpData != None:
            return ReadSheet(wb, backUpData)
        else:
            return None
    # print(sht.max_row)
    # print(sht.max_column)
    dataList = []
    for row in sht.rows:
        rowData = []
        if row[0].value == None:
            continue
        for cell in row:
            rowData.append(cell.value)
        dataList.append(rowData)
    del dataList[0] # del title
    # print(dataList[0])
    # print(dataList[1])
    # print(dataList[-1])
    return dataList

def RemoveDupLib4FullResult(frList):
    frDict = {}
    for fr in frList:
        if fr[0] not in frDict:
            frDict[fr[0]] = []
        frDict[fr[0]].append(fr[1:])
    # print(len(frDict))
    return frDict

def RemoveDupSym(frDict):
    symDict = {}
    for libName, descriptions in frDict.items():
        tmpDict = {}
        for des in descriptions:
            if des[0] not in tmpDict:
                tmpDict[des[0]] = {'descriptions' : des[1 : 7], 'locations' : []}
            tmpDict[des[0]]['locations'].append(des[7] + " : " + str(des[8]))
        symDict[libName] = tmpDict
    # print(len(symDict))
    return symDict

def WriteRiskSummary2xlsx(wb, symDict, sheetName = 'risk_summary'):
    sheets = wb.sheetnames
    if sheetName in sheets:
        wb.remove(wb[sheetName])
    rssheet = wb.create_sheet(sheetName, 0)
    #表头
    rssheet.cell(row = 1, column = 1, value = '三方库名')
    rssheet.cell(row = 1, column = 2, value = '符号名')
    rssheet.cell(row = 1, column = 3, value = '类型')
    rssheet.cell(row = 1, column = 4, value = '头文件')
    rssheet.cell(row = 1, column = 5, value = '所属模块')
    rssheet.cell(row = 1, column = 6, value = '状态')
    rssheet.cell(row = 1, column = 7, value = 'OH替换')
    rssheet.cell(row = 1, column = 8, value = '备注')
    rssheet.cell(row = 1, column = 9, value = '出现位置')
    rssheet.cell(row = 1, column = 10, value = '出现数量')
    i = 2
    dims = {1 : 8, 2 : 6, 3 : 4, 4 : 6, 5 : 8, 6 : 4, 7 : 6, 8 : 10, 9 : 8, 10 : 8}
    for libName, val in symDict.items():  
        for sym, data in val.items():
            rssheet.cell(row = i, column = 1, value = libName)
            dims[1] = len(libName) if dims[1] < len(libName) else dims[1]
            rssheet.cell(row = i, column = 2, value = sym)
            dims[2] = len(sym) if dims[2] < len(sym) else dims[2]
            rssheet.cell(row = i, column = 3, value = data['descriptions'][0])
            if data['descriptions'][0] != None:
                dims[3] = len(data['descriptions'][0]) if dims[3] < len(data['descriptions'][0]) else dims[3]
            rssheet.cell(row = i, column = 4, value = data['descriptions'][1])
            if data['descriptions'][1] != None:
                dims[4] = len(data['descriptions'][1]) if dims[4] < len(data['descriptions'][1]) else dims[4]
            rssheet.cell(row = i, column = 5, value = data['descriptions'][2])
            if data['descriptions'][2] != None:
                dims[5] = len(data['descriptions'][2]) if dims[5] < len(data['descriptions'][2]) else dims[5]
            rssheet.cell(row = i, column = 6, value = data['descriptions'][3])
            if data['descriptions'][3] != None:
                dims[6] = len(data['descriptions'][3]) if dims[6] < len(data['descriptions'][3]) else dims[6]
            rssheet.cell(row = i, column = 7, value = data['descriptions'][4])
            if data['descriptions'][4] != None:
                dims[7] = len(data['descriptions'][4]) if dims[7] < len(data['descriptions'][4]) else dims[7]
            rssheet.cell(row = i, column = 8, value = data['descriptions'][5])
            rssheet.cell(row = i, column = 9, value = str(data['locations']).replace("'", '"'))
            rssheet.cell(row = i, column = 10, value = len(data['locations']))
            i += 1
    for colum, width in dims.items():
        rssheet.column_dimensions[get_column_letter(colum)].width = width + 1
    return len(symDict)

def RemoveDupStatus(rsList):
    libDict = {}
    for rs in rsList:
        if rs[0] not in libDict:
            libDict[rs[0]] = []
                              #api    #状态  #个数
        libDict[rs[0]].append([rs[1], rs[5], rs[9]])
    # print(len(libDict))
    return libDict

def CountStatus(libDict):
    statusDict = {}
    for libName, apiStatus in libDict.items():
        # print(libName, '  ', len(apiStatus))
        tmpDict = {}
        for stat in apiStatus:
            if stat[1] not in tmpDict:
                tmpDict[stat[1]] = 0
            tmpDict[stat[1]] += 1
        statusDict[libName] = tmpDict
    # print(len(statusDict))
    for k,v in statusDict.items():
        print(k , " ", v)
    return statusDict

def WriteConclusion2Sheet(wb, statusDict, sheetName = 'conclusion'):
    sheets = wb.sheetnames
    if sheetName in sheets:
        wb.remove(wb[sheetName])
    csheet = wb.create_sheet(sheetName, 0)
    #表头
    csheet.cell(row = 1, column = 1, value = '三方库名')
    csheet.cell(row = 1, column = 2, value = '风险分析')
    csheet.cell(row = 1, column = 3, value = '风险项')
    csheet.cell(row = 1, column = 4, value = '风险项个数')
    csheet.cell(row = 1, column = 5, value = '三方库团队建议')
    csheet.cell(row = 1, column = 6, value = '预估工作量')
    i = 2
    dims = {1 : 6, 2 : 8, 3 : 6, 4 : 10, 5 : 14, 6 : 10}
    mergeCount = []
    for libName, val in statusDict.items():
        mergeCount.append(len(val))
        for stat, count in val.items():
            csheet.cell(row = i, column = 1, value = libName)
            dims[1] = len(libName) if dims[1] < len(libName) else dims[1]
            csheet.cell(row = i, column = 3, value = stat)
            dims[3] = len(stat) if dims[3] < len(stat) else dims[3]
            csheet.cell(row = i, column = 4, value = count)
            i += 1
    for colum, width in dims.items():
        csheet.column_dimensions[get_column_letter(colum)].width = width + 1
    mergeStart = 2
    for i in mergeCount:
        # print(i)
        # print("mergeStart ", mergeStart)
        if i == 1:
            mergeStart += i
            continue
        # print("mergeStart ", mergeStart)
        csheet.merge_cells("A" + str(mergeStart) + ":A" + str(mergeStart + i - 1))
        mergeStart = mergeStart + i

    return len(statusDict)

if __name__ == '__main__':
    excelFile = 'cocos_risk_assessments.xlsx'
    wb = openpyxl.load_workbook(excelFile)

    rsList = ReadSheet(wb, 'full_result', 'origin_data') # origin_data
    frlibDict = RemoveDupLib4FullResult(rsList)
    symDict = RemoveDupSym(frlibDict)
    WriteRiskSummary2xlsx(wb, symDict, 'risk_summary')

    rsList = ReadSheet(wb, 'risk_summary')
    libDict = RemoveDupStatus(rsList)
    statusDict = CountStatus(libDict)
    WriteConclusion2Sheet(wb, statusDict, 'conclusion')
    wb.save(excelFile)
    pass