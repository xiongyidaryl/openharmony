
from collections import defaultdict
import re

def remove_CCpp_Comment(text):
    """
    description: remove comments in source file for detection
    param： text, type: str
    """

    def blotOutNonNewlines(strIn):
        return "" + ("\n" * strIn.count('\n'))

    def replacer(match):
        s = match.group(0)
        if s.startswith('/'):
            return blotOutNonNewlines(s)
        else:
            return s

    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE
    )

    return re.sub(pattern, replacer, text)

def remove_comment(text):
    """
        description: remove comments in single line for detection
        param： text, type: str
    """
    pattern = re.compile(r'.*?(/\*.*?\*/)')
    return re.sub(pattern, '', text)

def find_api_in_source(source_file, target_apis, target_apis_dict):
    """
        description: find warning apis used in source file,
        params: text, type: str
        params: target_apis, type: list
        params: target_apis_dict, type: dict
        return: ret, types: dict -- keys: line detected warning in source file, values: apis info
    """
    ret = defaultdict(list)
    check_apis = []
    check_exist = 0
    with open(source_file, 'r+', encoding='UTF-8', errors='ignore') as f:
        content = f.read()
        new_content = remove_CCpp_Comment(content)
        f.close()
        for api in target_apis:
            if api in new_content and check_exist == 0:
                with open(source_file, 'w+', encoding='UTF-8', errors='ignore') as f:
                    f.write(new_content)
                    f.close()
                    check_exist = 1
    totalLines = 0
    with open(source_file, encoding='UTF-8', errors='ignore') as f:
        lines = f.readlines()
        totalLines += len(lines)
        line_count = 1
        for line in lines:
            line = remove_comment(line)
            for api in target_apis:
                if api not in check_apis and api_filter(api, line):
                    for key, values in target_apis_dict.items():
                        for m, n in values.items():
                            if api == m:
                                data_list = [(key, n)]
                                ret[line_count].append(data_list)
                                check_apis.append(api)
            line_count += 1
        f.close()
    if ret:
        print(f'Determined risk found in source file {source_file}')
        # print(ret)
    print(f'Risk scan file {source_file} over!')
    return ret, totalLines


def api_filter(api, line_content):

    if api + ' ' in line_content and (' ' + api in line_content or '=' + api in line_content or '\t' + api in line_content):
        return True
    elif api + '(' in line_content and (' ' + api in line_content or '=' + api in line_content or '\t' + api in line_content):
        return True
    elif api + ';' in line_content and (' ' + api in line_content or '=' + api in line_content):
        return True
    else:
        return False