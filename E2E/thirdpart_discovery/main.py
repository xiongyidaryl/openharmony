from get_class import get_class_in_homepage, get_page_in_homepage
from get_gitee_repos_in_class import get_gitee_repos_all
from get_github_repos_in_class import get_github_repos_all

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Gitee数据抓取步骤为8-12行，如需抓取gitee取消注释即可
    # classify = get_class_in_homepage()
    # get_gitee_repos_all(classify)
    # gitee_language_list = [('C', 'https://gitee.com/explore/all?lang=C&order=starred'), ('C++', 'https://gitee.com/explore/all?lang=cpp&order=starred')]
    # page_list = get_page_in_homepage(gitee_language_list)
    # get_gitee_repos_all(gitee_language_list, page_list)
    github_language_list = [('C', 'https://api.github.com/search/repositories?q=stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++', 'https://api.github.com/search/repositories?q=stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100'),
                            ('C_topic_android', 'https://api.github.com/search/repositories?q=topic%3Aandroid+stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++_topic_android', 'https://api.github.com/search/repositories?q=topic%3Aandroid+stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100'),
                            ('C_topic_ios',
                             'https://api.github.com/search/repositories?q=topic%3Aios+stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++_topic_ios',
                             'https://api.github.com/search/repositories?q=topic%3Aios+stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100'),
                            ('C_name_android',
                             'https://api.github.com/search/repositories?q=in%3Aname+android+stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++_name_android',
                             'https://api.github.com/search/repositories?q=in%3Aname+android+stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100'),
                            ('C_name_ios',
                             'https://api.github.com/search/repositories?q=in%3Aname+ios+stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++_name_ios',
                             'https://api.github.com/search/repositories?q=in%3Aname+ios+stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100'),
                            ('C_readme_android',
                             'https://api.github.com/search/repositories?q=in%3Areadme+android+stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++_readme_android',
                             'https://api.github.com/search/repositories?q=in%3Areadme+android+stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100'),
                            ('C_readme_ios',
                             'https://api.github.com/search/repositories?q=in%3Areadme+ios+stars%3A>10+language:C&sort=created&order=asc&per_page=100'),
                            ('C++_readme_ios',
                             'https://api.github.com/search/repositories?q=in%3Areadme+ios+stars%3A>10+language:C%2B%2B&sort=created&order=asc&per_page=100')
                            ]
    get_github_repos_all(github_language_list)



