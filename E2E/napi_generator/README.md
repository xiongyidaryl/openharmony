# NDK框架生成工具验证

### 背景

知识体系C/C++三方库生态中，应用如何调用C/C++的库是一个比较常态的问题。针对该问题，深开鸿研发并开源了一套NAPI框架生成的工具。

### 工具验证

#### 工具获取

- 通过命令生成工具

  NAPI框架生成工具获取地址：[NAPI框架生成工具](https://gitee.com/openharmony/napi_generator).

  编译生成工具的命令用法如下：

  

  ```
  # npm i typescript					// 在napi_generator/src目录下执行命令
  # npm i stdio						// 在napi_generator目录下执行命令
  # sudo npm i -g pkg					// 在napi_generator目录下执行命令
  # pkg -t node14-linux . -o napi_generator-linux	// 在napi_generator目录下执行命令
  ```

  使用该命令前需要配置相关的环境，具体参照文档:[工具开发说明](https://gitee.com/openharmony/napi_generator/blob/master/docs/DEVELOP_ZH.md).

  

- 使用现有的工具（[下载链接](http://ftpkaihongdigi.i234.me:5000/sharing/GGsW3B68u))

#### 待验证项目TS文件

目前使用的是yoga三方库的napi接口。经过yoga原生库js测试用例分析出需要移植的所有NAPI接口

包含2种方式：

- API以function方式定义：[yoga_api.1](./res/@ohos.YogaApi1.d.ts)

- API带有interface接口方式定义:[yoga_api.2](./res/@ohos.YogaApi2.d.ts)

#### 验证方法

- 此工具验证基于OpenHarmony3.2 beta1

- 准备好工具与NAPI接口文件后，执行以下指令(工具的具体使用说明参照:[NAPI框架生成工具使用说明](https://gitee.com/openharmony/napi_generator/blob/master/docs/INSTRUCTION_ZH.md))：

  ```
  ./napi_generator-linux -f @ohos.YogaApi.d.ts -o out
  ```

- 执行完后回在out目录生成对应的ndk工程文件：

  ![ndk_content](res/out.png)

  注意：如果没有生成对应的源文件，那可能是NAPI接口文件定义有误，具体信息可以查看napi_gen.log文件。

#### 验证结果

对于格式正确的napi接口文件，napi_generator工具可以正常生成ndk框架文件，但有一些问题需要修正：

- 生成的gn文件需要调整

  1.ace_napi路径在3.2上已做了调整，需将

  ```
  deps=[ "//foundation/ace/napi:ace_napi", ]
  ```

  修改为

  ```
  deps=[ "//foundation/arkui/napi:ace_napi", ]
  ```

  2.flag配置以及对应的宏定义开关需要手动配置

  

- 当定义的接口返回类型是void类型，中间代码调用的时候会多传入一个&out的参数，该问题已提[issue](https://gitee.com/openharmony/napi_generator/issues/I5MCHO?from=project-issue)，且深开鸿有回复正在优化此bug。（20220830版本已修复该问题。）

  ![输入图片说明](https://foruda.gitee.com/images/1660547866622727581/21.png)

   

- NAPI的参数以及返回值类型只支持基础类型(number, string, boolean), 不支持object类型。

    

- 整形数据类型转换全部是32位的

    如果是在64位系统上运行，整形数据转换需要转成64位，工具没有提供相应的设置，只能手动修改。关于这点已经反馈到相应人员，后期可以通过参数方式来设置。

    

- NAPI接口文件带interface类型接口
  1. 能将interface识别成类并封装对应的接口。
  2. 返回值为interface的话，interface中的变量只能是基础类型(number,string,boolean)，不支持object。
  3. 针对与需要返回类对象到应用端的需求，工具开发人员回复暂不支持，后续是否支持需要做一下用户调研，常用的场景会纳入到需求中。



### 结论

- ndk框架生成工具可以生成简单的napi接口，且参数不能涉及到object。

- 如果napi需要封装对应的类传递到应用端，使应用可以直接通过类对象调用类的方法，该工具无法实现。

  







