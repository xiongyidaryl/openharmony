/*
* Copyright (C) 2022 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import {AsyncCallback, Callback} from "./basic";

/**
 * Provides YOGA APIs
 *
 * @since 8
 * @syscap SystemCapability.YogaApi
 */
declare namespace YOGA {

	/**********YOGA config 相关*************/
	/**
	* @brief 创建YGConfig
	* @param
	* @return 返回config指针的地址(long 类型)
	*/
	function YGConfigNewMethod(callback: AsyncCallback<number>):void;
	function YGConfigNewMethod():Promise<number>;
	function YGConfigNewMethodSync():number;

	/**
    * @brief 创建YGConfig
	* @param nativePointer: config指针的地址(long 类型)
	* @return nullptr;
    */
	function YGConfigFreeMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGConfigFreeMethod(nativePointer:number):Promise<void>;
	function YGConfigFreeMethodSync(nativePointer:number):void;
	
	/**
    * @brief 使能 ExperimentalFeature
	* @param nativePointer: config指针的地址(long 类型), feature: 功能选项,默认值为0(枚举类型，取值参照 @YGEnums.h), enable--使能标志(bool 类型)
	* @return
    */
	function YGConfigSetExperimentalFeatureEnabledMethod(nativePointer:number, feature:number, enable:boolean, callback: AsyncCallback<void>):void;
	function YGConfigSetExperimentalFeatureEnabledMethod(nativePointer:number, feature:number, enable:boolean):Promise<void>;
	function YGConfigSetExperimentalFeatureEnabledMethodSync(nativePointer:number, feature:number, enable:boolean):void;
	
	/**
    * @brief 设置是否使用默认web
	* @param nativePointer: config指针的地址(long 类型), useWebDefaults: 是否使用WebDefaults(bool 类型)
	* @return
    */
	function YGConfigSetUseWebDefaultsMethod(nativePointer:number, useWebDefaults:boolean, callback: AsyncCallback<void>):void;
	function YGConfigSetUseWebDefaultsMethod(nativePointer:number, useWebDefaults:boolean):Promise<void>;
	function YGConfigSetUseWebDefaultsMethodSync(nativePointer:number, useWebDefaults:boolean):void;
	
	/**
    * @brief 设置是否打印
	* @param nativePointer: config指针的地址(long 类型), flag: 打印标志(bool 类型)
	* @return
    */
	function YGConfigSetPrintTreeFlagMethod(nativePointer:number, flag:boolean, callback: AsyncCallback<void>):void;
	function YGConfigSetPrintTreeFlagMethod(nativePointer:number, flag:boolean):Promise<void>;
	function YGConfigSetPrintTreeFlagMethodSync(nativePointer:number, flag:boolean):void;
	
	/**
    * @brief 设置像素点缩放因子
	* @param nativePointer: config指针的地址(long 类型), pixelsInPoint: 缩放因子(float 类型)
	* @return
    */
	function YGConfigSetPointScaleFactorMethod(nativePointer:number, pixelsInPoint:number, callback: AsyncCallback<void>):void;
	function YGConfigSetPointScaleFactorMethod(nativePointer:number, pixelsInPoint:number):Promise<void>;
	function YGConfigSetPointScaleFactorMethodSync(nativePointer:number, pixelsInPoint:number):void;
	
	/**
    * @brief 设置是否使用拉伸方式
	* @param nativePointer: config指针的地址(long 类型), useLegacyStretchBehaviour:是否使用拉伸方式(bool 类型)
	* @return
    */
	function YGConfigSetUseLegacyStretchBehaviourMethod(nativePointer:number, useLegacyStretchBehaviour:boolean, callback: AsyncCallback<void>):void;
	function YGConfigSetUseLegacyStretchBehaviourMethod(nativePointer:number, useLegacyStretchBehaviour:boolean):Promise<void>;
	function YGConfigSetUseLegacyStretchBehaviourMethodSync(nativePointer:number, useLegacyStretchBehaviour:boolean):void;
	
	/**
    * @brief 使能不同布局使用拉伸方式
	* @param nativePointer: config指针的地址(long 类型), enable:使能标志(bool 类型)
	* @return
    */
	function YGConfigSetShouldDiffLayoutWithoutLegacyStretchBehaviourMethod(nativePointer:number, enable:boolean, callback: AsyncCallback<void>):void;
	function YGConfigSetShouldDiffLayoutWithoutLegacyStretchBehaviourMethod(nativePointer:number, enable:boolean):Promise<void>;
	function YGConfigSetShouldDiffLayoutWithoutLegacyStretchBehaviourMethodSync(nativePointer:number, enable:boolean):void;
	
	/**
    * @brief 设置打印回调函数
	* @param nativePointer: config指针的地址(long 类型), logger:打印回调对象，具体实现参照YogaLogger定义(object 类型)
	* @return
    */
	/*
	function YGConfigSetLogger(nativePointer:number, logger:object, callback: AsyncCallback<void>):void;
	function YGConfigSetLogger(nativePointer:number, log:object):Promise<void>;
	*/
	
	/**********YOGA config 相关*************/
	/**
    * @brief 新建Node
	* @param
	* @return 返回Node指针的地址(long 类型)
    */
	function YGNodeNewMethod(callback: AsyncCallback<number>):void;
	function YGNodeNewMethod():Promise<number>;
	function YGNodeNewMethodSync():number;
	
	/**
    * @brief 新建Node并设置config
	* @param configPointer：config指针的地址(long 类型)
	* @return 返回Node指针的地址(long 类型)
    */
	function YGNodeNewWithConfigMethod(configPointer:number, callback: AsyncCallback<number>):void;
	function YGNodeNewWithConfigMethod(configPointer:number):Promise<number>;
	function YGNodeNewWithConfigMethodSync(configPointer:number):number;
	
	/**
    * @brief 释放Node资源
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeFreeMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeFreeMethod(nativePointer:number):Promise<void>;
	function YGNodeFreeMethodSync(nativePointer:number):void;
	
	/**
    * @brief 重置Node配置
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeResetMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeResetMethod(nativePointer:number):Promise<void>;
	function YGNodeResetMethodSync(nativePointer:number):void;
	
	/**
    * @brief Node插入子节点
	* @param nativePointer：Node指针的地址(long 类型)，childPointer：子节点指针地址(long 类型)，index：子节点插入位置的索引(int 类型)
	* @return
    */
	function YGNodeInsertChildMethod(nativePointer:number, childPointer:number, index:number, callback: AsyncCallback<void>):void;
	function YGNodeInsertChildMethod(nativePointer:number, childPointer:number, index:number):Promise<void>;
	function YGNodeInsertChildMethodSync(nativePointer:number, childPointer:number, index:number):void;
	
	/**
    * @brief Node交换子节点
	* @param nativePointer：Node指针的地址(long 类型)，childPointer：子节点指针地址(long 类型)，index：子节点交换位置的索引(int 类型)
	* @return
    */
	function YGNodeSwapChildMethod(nativePointer:number, childPointer:number, index:number, callback: AsyncCallback<void>):void;
	function YGNodeSwapChildMethod(nativePointer:number, childPointer:number, index:number):Promise<void>;
	function YGNodeSwapChildMethodSync(nativePointer:number, childPointer:number, index:number):void;
	
	/**
    * @brief 设置Node是否参考基线
	* @param nativePointer：Node指针的地址(long 类型)，isReferenceBaseline：是否参考基线标志(bool 类型)
	* @return
    */
	function YGNodeSetIsReferenceBaselineMethod(nativePointer:number, isReferenceBaseline:boolean, callback: AsyncCallback<void>):void;
	function YGNodeSetIsReferenceBaselineMethod(nativePointer:number, isReferenceBaseline:boolean):Promise<void>;
	function YGNodeSetIsReferenceBaselineMethodSync(nativePointer:number, isReferenceBaseline:boolean):void;

	/**
    * @brief 获取Node是否参考基线
	* @param nativePointer：Node指针的地址(long 类型
	* @return 是否参考基线标志(bool 类型)
    */
	function YGNodeIsReferenceBaselineMethod(nativePointer:number, callback: AsyncCallback<boolean>):void;
	function YGNodeIsReferenceBaselineMethod(nativePointer:number):Promise<boolean>;
	function YGNodeIsReferenceBaselineMethodSync(nativePointer:number):boolean;

	/**
    * @brief 清除Node所有子节点
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeClearChildrenMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeClearChildrenMethod(nativePointer:number):Promise<void>;
	function YGNodeClearChildrenMethodSync(nativePointer:number):void;

	/**
    * @brief 移除Node子节点
	* @param nativePointer：Node指针的地址(long 类型), childPointer: 子节点指针地址(long 类型)
	* @return
    */
	function YGNodeRemoveChildMethod(nativePointer:number, childPointer:number, callback: AsyncCallback<void>):void;
	function YGNodeRemoveChildMethod(nativePointer:number, childPointer:number):Promise<void>;
	function YGNodeRemoveChildMethodSync(nativePointer:number, childPointer:number):void;

	/**
    * @brief 计算Node布局
	* @param nativePointer：Node指针的地址(long 类型), width:布局宽(float 类型), height:布局高(float 类型),nativePointers:布局中所有Node的指针地址数组(long[] 类型),javaNodes:应用层node数组(object[] 类型)
	* @return
    */
	function YGNodeCalculateLayoutMethod(nativePointer:number, width:number, height:number, nativePointers:Array<number>, javaNodes:Array<number>, callback: AsyncCallback<void>):void;
	function YGNodeCalculateLayoutMethod(nativePointer:number, width:number, height:number, nativePointers:Array<number>, javaNodes:Array<number>):Promise<void>;
	function YGNodeCalculateLayoutMethodSync(nativePointer:number, width:number, height:number, nativePointers:Array<number>, javaNodes:Array<number>):void;
	
	
	/**
    * @brief 标记Node污点 // ？？
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeMarkDirtyMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeMarkDirtyMethod(nativePointer:number):Promise<void>;
	function YGNodeMarkDirtyMethodSync(nativePointer:number):void;
	
	/**
    * @brief 标记污点并继承到下一个Node // ？？
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeMarkDirtyAndPropogateToDescendantsMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeMarkDirtyAndPropogateToDescendantsMethod(nativePointer:number):Promise<void>;
	function YGNodeMarkDirtyAndPropogateToDescendantsMethodSync(nativePointer:number):void;
	
	/**
    * @brief 获取Node是否为污点
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 是否为污点(bool 类型)
    */
	function YGNodeIsDirtyMethod(nativePointer:number, callback: AsyncCallback<boolean>):void;
	function YGNodeIsDirtyMethod(nativePointer:number):Promise<boolean>;
	function YGNodeIsDirtyMethodSync(nativePointer:number):boolean;
	
	/**
    * @brief 拷贝Node的样式
	* @param dstNativePointer：拷贝源Node指针的地址(long 类型), dstNativePointer：目标Node指针的地址(long 类型)
	* @return
    */
	function YGNodeCopyStyleMethod(dstNativePointer:number, srcNativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeCopyStyleMethod(dstNativePointer:number, srcNativePointer:number):Promise<void>;
	function YGNodeCopyStyleMethodSync(dstNativePointer:number, srcNativePointer:number):void;

	/**
    * @brief 获取Node样式的方向
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 布局方向，取值参照@YGEnums.h中的YGDirection枚举
    */
	function YGNodeStyleGetDirectionMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetDirectionMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetDirectionMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的方向
	* @param nativePointer：Node指针的地址(long 类型), direction:布局方向，取值参照@YGEnums.h中的YGDirection枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetDirectionMethod(nativePointer:number, direction:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetDirectionMethod(nativePointer:number, direction:number):Promise<void>;
	function YGNodeStyleSetDirectionMethodSync(nativePointer:number, direction:number):void;
	
	/**
    * @brief 获取Node样式的flex方向
	* @param nativePointer：Node指针的地址(long 类型)
	* @return flex方向，取值参照@YGEnums.h中的YGFlexDirection枚举
    */
	function YGNodeStyleGetFlexDirectionMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetFlexDirectionMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetFlexDirectionMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的flex方向
	* @param nativePointer：Node指针的地址(long 类型), flexDirection:flex方向，取值参照@YGEnums.h中的YGFlexDirection枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetFlexDirectionMethod(nativePointer:number, flexDirection:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexDirectionMethod(nativePointer:number, flexDirection:number):Promise<void>;
	function YGNodeStyleSetFlexDirectionMethodSync(nativePointer:number, flexDirection:number):void;
	
	/**
    * @brief 获取Node中容器的样式
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 容器的样式值，取值参照@YGEnums.h中的YGJustify枚举
    */
	function YGNodeStyleGetJustifyContentMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetJustifyContentMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetJustifyContentMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node中容器的样式
	* @param nativePointer：Node指针的地址(long 类型), justifyContent:容器的样式，取值参照@YGEnums.h中的YGJustify枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetJustifyContentMethod(nativePointer:number, justifyContent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetJustifyContentMethod(nativePointer:number, justifyContent:number):Promise<void>;
	function YGNodeStyleSetJustifyContentMethodSync(nativePointer:number, justifyContent:number):void;
	
	/**
    * @brief 获取Node样式排列方式
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 排列方式，取值参照@YGEnums.h中的YGAlign枚举
    */
	function YGNodeStyleGetAlignItemsMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetAlignItemsMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetAlignItemsMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式排列方式
	* @param nativePointer：Node指针的地址(long 类型), alignItems:容器的样式，取值参照@YGEnums.h中的YGAlign枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetAlignItemsMethod(nativePointer:number, alignItems:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetAlignItemsMethod(nativePointer:number, alignItems:number):Promise<void>;
	function YGNodeStyleSetAlignItemsMethodSync(nativePointer:number, alignItems:number):void;
	
	/**
    * @brief 获取Node样式排列方式
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 排列方式，取值参照@YGEnums.h中的YGAlign枚举
    */
	function YGNodeStyleGetAlignSelfMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetAlignSelfMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetAlignSelfMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式排列方式
	* @param nativePointer：Node指针的地址(long 类型), alignSelf:容器的样式，取值参照@YGEnums.h中的YGAlign枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetAlignSelfMethod(nativePointer:number, alignSelf:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetAlignSelfMethod(nativePointer:number, alignSelf:number):Promise<void>;
	function YGNodeStyleSetAlignSelfMethodSync(nativePointer:number, alignSelf:number):void;
	
	/**
    * @brief 获取Node样式排列方式
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 排列方式，取值参照@YGEnums.h中的YGAlign枚举
    */
	function YGNodeStyleGetAlignContentMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetAlignContentMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetAlignContentMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式排列方式
	* @param nativePointer：Node指针的地址(long 类型), alignContent:容器的样式，取值参照@YGEnums.h中的YGAlign枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetAlignContentMethod(nativePointer:number, alignContent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetAlignContentMethod(nativePointer:number, alignContent:number):Promise<void>;
	function YGNodeStyleSetAlignContentMethodSync(nativePointer:number, alignContent:number):void;
	
	/**
    * @brief 获取Node样式位置类型
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 位置类型，取值参照@YGEnums.h中的YGPositionType枚举
    */
	function YGNodeStyleGetPositionTypeMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetPositionTypeMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetPositionTypeMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式位置类型
	* @param nativePointer：Node指针的地址(long 类型), positionType:位置类型，取值参照@YGEnums.h中的YGPositionType枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetPositionTypeMethod(nativePointer:number, positionType:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetPositionTypeMethod(nativePointer:number, positionType:number):Promise<void>;
	function YGNodeStyleSetPositionTypeMethodSync(nativePointer:number, positionType:number):void;
	
	/**
    * @brief 获取Node样式的FlexWrap
	* @param nativePointer：Node指针的地址(long 类型)
	* @return FlexWrap值，取值参照@YGEnums.h中的YGWrap枚举
    */
	function YGNodeStyleGetFlexWrapMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetFlexWrapMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetFlexWrapMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的FlexWrap
	* @param nativePointer：Node指针的地址(long 类型), wrapType:FlexWrap值，取值参照@YGEnums.h中的YGWrap枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetFlexWrapMethod(nativePointer:number, wrapType:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexWrapMethod(nativePointer:number, wrapType:number):Promise<void>;
	function YGNodeStyleSetFlexWrapMethodSync(nativePointer:number, wrapType:number):void;
	
	/**
    * @brief 获取Node样式的Overflow
	* @param nativePointer：Node指针的地址(long 类型)
	* @return Overflow值，取值参照@YGEnums.h中的YGOverflow枚举
    */
	function YGNodeStyleGetOverflowMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetOverflowMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetOverflowMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的Overflow
	* @param nativePointer：Node指针的地址(long 类型), overflow: Overflow值，取值参照@YGEnums.h中的YGOverflow枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetOverflowMethod(nativePointer:number, overflow:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetOverflowMethod(nativePointer:number, overflow:number):Promise<void>;
	function YGNodeStyleSetOverflowMethodSync(nativePointer:number, overflow:number):void;
	
	/**
    * @brief 获取Node样式的显示方式
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 显示方式，取值参照@YGEnums.h中的YGDisplay枚举
    */
	function YGNodeStyleGetDisplayMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetDisplayMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetDisplayMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的显示方式
	* @param nativePointer：Node指针的地址(long 类型), display: 显示方式，取值参照@YGEnums.h中的YGDisplay枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetDisplayMethod(nativePointer:number, display:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetDisplayMethod(nativePointer:number, display:number):Promise<void>;
	function YGNodeStyleSetDisplayMethodSync(nativePointer:number, display:number):void;
	
	/**
    * @brief 获取Node样式弹性布局的弹性值
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 弹性值(float 类型)。
    */
	function YGNodeStyleGetFlexMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetFlexMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetFlexMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式弹性布局的弹性值
	* @param nativePointer：Node指针的地址(long 类型), flex: 弹性值(float 类型)
	* @return
    */
	function YGNodeStyleSetFlexMethod(nativePointer:number, flex:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexMethod(nativePointer:number, flex:number):Promise<void>;
	function YGNodeStyleSetFlexMethodSync(nativePointer:number, flex:number):void;
	
	/**
    * @brief 获取Node样式弹性布局的生长值
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 生长值(float 类型)。
    */
	function YGNodeStyleGetFlexGrowMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetFlexGrowMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetFlexGrowMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式弹性布局的生长值
	* @param nativePointer：Node指针的地址(long 类型), flexGrow: 生长值(float 类型)
	* @return
    */
	function YGNodeStyleSetFlexGrowMethod(nativePointer:number, flexGrow:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexGrowMethod(nativePointer:number, flexGrow:number):Promise<void>;
	function YGNodeStyleSetFlexGrowMethodSync(nativePointer:number, flexGrow:number):void;
	
	/**
    * @brief 获取Node样式弹性布局的收缩值
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 收缩值(float 类型)。
    */
	function YGNodeStyleGetFlexShrinkMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetFlexShrinkMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetFlexShrinkMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式弹性布局的收缩值
	* @param nativePointer：Node指针的地址(long 类型), flexShrink: 收缩值(float 类型)
	* @return
    */
	function YGNodeStyleSetFlexShrinkMethod(nativePointer:number, flexShrink:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexShrinkMethod(nativePointer:number, flexShrink:number):Promise<void>;
	function YGNodeStyleSetFlexShrinkMethodSync(nativePointer:number, flexShrink:number):void;
	
	/**
    * @brief 获取Node样式弹性布局的基础值
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 基础值(float 类型)。
    */
	function YGNodeStyleGetFlexBasisMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetFlexBasisMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetFlexBasisMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式弹性布局的基础值
	* @param nativePointer：Node指针的地址(long 类型), flexBasis: 基础值(float 类型)
	* @return
    */
	function YGNodeStyleSetFlexBasisMethod(nativePointer:number, flexBasis:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexBasisMethod(nativePointer:number, flexBasis:number):Promise<void>;
	function YGNodeStyleSetFlexBasisMethodSync(nativePointer:number, flexBasis:number):void;
	
	/**
    * @brief 设置Node样式弹性布局的基础百分比
	* @param nativePointer：Node指针的地址(long 类型), percent: 基础百分比(float 类型)
	* @return 0 success, others failed;
    */
	function YGNodeStyleSetFlexBasisPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleSetFlexBasisPercentMethod(nativePointer:number, percent:number):Promise<number>;
	function YGNodeStyleSetFlexBasisPercentMethodSync(nativePointer:number, percent:number):number;
	
	/**
    * @brief 设置Node样式自动适配弯曲值
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeStyleSetFlexBasisAutoMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetFlexBasisAutoMethod(nativePointer:number):Promise<void>;
	function YGNodeStyleSetFlexBasisAutoMethodSync(nativePointer:number):void;
	
	/**
    * @brief 获取Node样式的外边距
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型)
	* @return 外边距值(float 类型)
    */
	function YGNodeStyleGetMarginMethod(nativePointer:number, edge:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetMarginMethod(nativePointer:number, edge:number):Promise<number>;
	function YGNodeStyleGetMarginMethodSync(nativePointer:number, edge:number):number;
	
	/**
    * @brief 设置Node样式的外边距值
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型),margin:外边距值(float 类型)
	* @return
    */
	function YGNodeStyleSetMarginMethod(nativePointer:number, edge:number, margin:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMarginMethod(nativePointer:number, edge:number, margin:number):Promise<void>;
	function YGNodeStyleSetMarginMethodSync(nativePointer:number, edge:number, margin:number):void;
	
	/**
    * @brief 设置Node样式的外边距百分比
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型),percent:外边距百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetMarginPercentMethod(nativePointer:number, edge:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMarginPercentMethod(nativePointer:number, edge:number, percent:number):Promise<void>;
	function YGNodeStyleSetMarginPercentMethodSync(nativePointer:number, edge:number, percent:number):void;
	
	/**
    * @brief 设置Node样式的自动适配外边距
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型)
	* @return
    */
	function YGNodeStyleSetMarginAutoMethod(nativePointer:number, edge:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMarginAutoMethod(nativePointer:number, edge:number):Promise<void>;
	function YGNodeStyleSetMarginAutoMethodSync(nativePointer:number, edge:number):void;
	
	/**
    * @brief 获取Node样式的内边距
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型)
	* @return 内边距值(float 类型)
    */
	function YGNodeStyleGetPaddingMethod(nativePointer:number, edge:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetPaddingMethod(nativePointer:number, edge:number):Promise<number>;
	function YGNodeStyleGetPaddingMethodSync(nativePointer:number, edge:number):number;
	
	/**
    * @brief 设置Node样式的内边距
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型), padding:内边距值(float 类型)
	* @return
    */
	function YGNodeStyleSetPaddingMethod(nativePointer:number, edge:number, padding:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetPaddingMethod(nativePointer:number, edge:number, padding:number):Promise<void>;
	function YGNodeStyleSetPaddingMethodSync(nativePointer:number, edge:number, padding:number):void;
	
	/**
    * @brief 设置Node样式的内边距百分比
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型), percent:内边距值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetPaddingPercentMethod(nativePointer:number, edge:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetPaddingPercentMethod(nativePointer:number, edge:number, percent:number):Promise<void>;
	function YGNodeStyleSetPaddingPercentMethodSync(nativePointer:number, edge:number, percent:number):void;
	
	/**
    * @brief 获取Node样式的边框
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型)
	* @return 边框值(float 类型)
    */
	function YGNodeStyleGetBorderMethod(nativePointer:number, edge:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetBorderMethod(nativePointer:number, edge:number):Promise<number>;
	function YGNodeStyleGetBorderMethodSync(nativePointer:number, edge:number):number;
	
	/**
    * @brief 设置Node样式的边框
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型), border:边框值(float 类型)
	* @return
    */
	function YGNodeStyleSetBorderMethod(nativePointer:number, edge:number, border:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetBorderMethod(nativePointer:number, edge:number, border:number):Promise<void>;
	function YGNodeStyleSetBorderMethodSync(nativePointer:number, edge:number, border:number):void;
	
	/**
    * @brief 获取Node样式的位置
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型)
	* @return 位置值(float 类型)
    */
	function YGNodeStyleGetPositionMethod(nativePointer:number, edge:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetPositionMethod(nativePointer:number, edge:number):Promise<number>;
	function YGNodeStyleGetPositionMethodSync(nativePointer:number, edge:number):number;
	
	/**
    * @brief 设置Node样式的位置
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型), position:位置值(float 类型)
	* @return
    */
	function YGNodeStyleSetPositionMethod(nativePointer:number, edge:number, position:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetPositionMethod(nativePointer:number, edge:number, position:number):Promise<void>;
	function YGNodeStyleSetPositionMethodSync(nativePointer:number, edge:number, position:number):void;
	
	/**
    * @brief 设置Node样式的位置的百分比
	* @param nativePointer：Node指针的地址(long 类型), edge:边缘位置,值参照@YGEnums.h中的YGEdge枚举(int 类型), percent:位置值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetPositionPercentMethod(nativePointer:number, edge:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetPositionPercentMethod(nativePointer:number, edge:number, percent:number):Promise<void>;
	function YGNodeStyleSetPositionPercentMethodSync(nativePointer:number, edge:number, percent:number):void;
	
	/**
    * @brief 获取Node样式的宽度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 宽度值(float 类型)
    */
	function YGNodeStyleGetWidthMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetWidthMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetWidthMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的宽度
	* @param nativePointer：Node指针的地址(long 类型), width:宽度值(float 类型)
	* @return
    */
	function YGNodeStyleSetWidthMethod(nativePointer:number, width:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetWidthMethod(nativePointer:number, width:number):Promise<void>;
	function YGNodeStyleSetWidthMethodSync(nativePointer:number, width:number):void;
	
	/**
    * @brief 设置Node样式的宽度百分比
	* @param nativePointer：Node指针的地址(long 类型), percent:宽度值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetWidthPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetWidthPercentMethod(nativePointer:number, percent:number):Promise<void>;
	function YGNodeStyleSetWidthPercentMethodSync(nativePointer:number, percent:number):void;
	
	/**
    * @brief 设置Node样式的自适应宽度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeStyleSetWidthAutoMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetWidthAutoMethod(nativePointer:number):Promise<void>;
	function YGNodeStyleSetWidthAutoMethodSync(nativePointer:number):void;

	/**
    * @brief 获取Node样式的高度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 高度值(float 类型)
    */
	function YGNodeStyleGetHeightMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetHeightMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetHeightMethodSync(nativePointer:number):number;

	/**
    * @brief 设置Node样式的高度
	* @param nativePointer：Node指针的地址(long 类型), height:高度值(float 类型)
	* @return
    */
	function YGNodeStyleSetHeightMethod(nativePointer:number, height:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetHeightMethod(nativePointer:number, height:number):Promise<void>;
	function YGNodeStyleSetHeightMethodSync(nativePointer:number, height:number):void;
	
	/**
    * @brief 设置Node样式的高度百分比
	* @param nativePointer：Node指针的地址(long 类型), percent:高度值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetHeightPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetHeightPercentMethod(nativePointer:number, percent:number):Promise<void>;
	function YGNodeStyleSetHeightPercentMethodSync(nativePointer:number, percent:number):void;
	
	/**
    * @brief 设置Node样式的自适应高度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodeStyleSetHeightAutoMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetHeightAutoMethod(nativePointer:number):Promise<void>;
	function YGNodeStyleSetHeightAutoMethodSync(nativePointer:number):void;
	
	/**
    * @brief 获取Node样式的最小宽度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 最小宽度值(float 类型)
    */
	function YGNodeStyleGetMinWidthMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetMinWidthMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetMinWidthMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的最小宽度
	* @param nativePointer：Node指针的地址(long 类型), minWidth:最小宽度值(float 类型)
	* @return
    */
	function YGNodeStyleSetMinWidthMethod(nativePointer:number, minWidth:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMinWidthMethod(nativePointer:number, minWidth:number):Promise<void>;
	function YGNodeStyleSetMinWidthMethodSync(nativePointer:number, minWidth:number):void;
	
	/**
    * @brief 设置Node样式的最小宽度百分比
	* @param nativePointer：Node指针的地址(long 类型), percent:最小宽度值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetMinWidthPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMinWidthPercentMethod(nativePointer:number, percent:number):Promise<void>;
	function YGNodeStyleSetMinWidthPercentMethodSync(nativePointer:number, percent:number):void;
	
	/**
    * @brief 获取Node样式的最小高度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 最小高度值(float 类型)
    */
	function YGNodeStyleGetMinHeightMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetMinHeightMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetMinHeightMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的最小高度
	* @param nativePointer：Node指针的地址(long 类型), minHeight:最小高度值(float 类型)
	* @return
    */
	function YGNodeStyleSetMinHeightMethod(nativePointer:number, minHeight:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMinHeightMethod(nativePointer:number, minHeight:number):Promise<void>;
	function YGNodeStyleSetMinHeightMethodSync(nativePointer:number, minHeight:number):void;
	
	/**
    * @brief 设置Node样式的最小高度百分比
	* @param nativePointer：Node指针的地址(long 类型), percent:最小高度值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetMinHeightPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMinHeightPercentMethod(nativePointer:number, percent:number):Promise<void>;
	function YGNodeStyleSetMinHeightPercentMethodSync(nativePointer:number, percent:number):void;

	/**
    * @brief 获取Node样式的最大宽度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 最大宽度值(float 类型)
    */
	function YGNodeStyleGetMaxWidthMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetMaxWidthMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetMaxWidthMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的最大宽度
	* @param nativePointer：Node指针的地址(long 类型), maxWidth:最大宽度值(float 类型)
	* @return
    */
	function YGNodeStyleSetMaxWidthMethod(nativePointer:number, maxWidth:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMaxWidthMethod(nativePointer:number, maxWidth:number):Promise<void>;
	function YGNodeStyleSetMaxWidthMethodSync(nativePointer:number, maxWidth:number):void;
	
	/**
    * @brief 设置Node样式的最大宽度百分比
	* @param nativePointer：Node指针的地址(long 类型), percent:最大宽度值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetMaxWidthPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMaxWidthPercentMethod(nativePointer:number, percent:number):Promise<void>;
	function YGNodeStyleSetMaxWidthPercentMethodSync(nativePointer:number, percent:number):void;
	
	/**
    * @brief 获取Node样式的最大高度
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 最大高度值(float 类型)
    */
	function YGNodeStyleGetMaxHeightMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetMaxHeightMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetMaxHeightMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的最大高度
	* @param nativePointer：Node指针的地址(long 类型), maxheight:最大高度值(float 类型)
	* @return
    */
	function YGNodeStyleSetMaxHeightMethod(nativePointer:number, maxheight:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMaxHeightMethod(nativePointer:number, maxheight:number):Promise<void>;
	function YGNodeStyleSetMaxHeightMethodSync(nativePointer:number, maxheight:number):void;
	
	/**
    * @brief 设置Node样式的最大高度百分比
	* @param nativePointer：Node指针的地址(long 类型), percent:最大高度值百分比(float 类型)
	* @return
    */
	function YGNodeStyleSetMaxHeightPercentMethod(nativePointer:number, percent:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetMaxHeightPercentMethod(nativePointer:number, percent:number):Promise<void>;
	function YGNodeStyleSetMaxHeightPercentMethodSync(nativePointer:number, percent:number):void;
	
	/**
    * @brief 获取Node样式的纵横比
	* @param nativePointer：Node指针的地址(long 类型)
	* @return 纵横比值(float 类型)
    */
	function YGNodeStyleGetAspectRatioMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeStyleGetAspectRatioMethod(nativePointer:number):Promise<number>;
	function YGNodeStyleGetAspectRatioMethodSync(nativePointer:number):number;
	
	/**
    * @brief 设置Node样式的纵横比
	* @param nativePointer：Node指针的地址(long 类型), aspectRatio:纵横比值(float 类型)
	* @return
    */
	function YGNodeStyleSetAspectRatioMethod(nativePointer:number, aspectRatio:number, callback: AsyncCallback<void>):void;
	function YGNodeStyleSetAspectRatioMethod(nativePointer:number, aspectRatio:number):Promise<void>;
	function YGNodeStyleSetAspectRatioMethodSync(nativePointer:number, aspectRatio:number):void;
	
	/**
    * @brief 使能MeasureFunc
	* @param nativePointer：Node指针的地址(long 类型), hasMeasureFunc:使能标志(bool 类型)
	* @return
    */
	function YGNodeSetHasMeasureFuncMethod(nativePointer:number, hasMeasureFunc:boolean, callback: AsyncCallback<void>):void;
	function YGNodeSetHasMeasureFuncMethod(nativePointer:number, hasMeasureFunc:boolean):Promise<void>;
	function YGNodeSetHasMeasureFuncMethodSync(nativePointer:number, hasMeasureFunc:boolean):void;
	
	/**
    * @brief 使能BaselineFunc
	* @param nativePointer：Node指针的地址(long 类型), hasMeasureFunc:使能标志(bool 类型)
	* @return
    */
	function YGNodeSetHasBaselineFuncMethod(nativePointer:number, hasMeasureFunc:boolean, callback: AsyncCallback<void>):void;
	function YGNodeSetHasBaselineFuncMethod(nativePointer:number, hasMeasureFunc:boolean):Promise<void>;
	function YGNodeSetHasBaselineFuncMethodSync(nativePointer:number, hasMeasureFunc:boolean):void;
	
	/**
    * @brief 打印Node信息
	* @param nativePointer：Node指针的地址(long 类型)
	* @return
    */
	function YGNodePrintMethod(nativePointer:number, callback: AsyncCallback<void>):void;
	function YGNodePrintMethod(nativePointer:number):Promise<void>;
	function YGNodePrintMethodSync(nativePointer:number):void;

	/**
    * @brief 复制 Node节点
	* @param nativePointer：Node指针的地址(long 类型)
	* @return Node指针地址(long 类型)
    */
	function YGNodeCloneMethod(nativePointer:number, callback: AsyncCallback<number>):void;
	function YGNodeCloneMethod(nativePointer:number):Promise<number>;
	function YGNodeCloneMethodSync(nativePointer:number):number;
}
