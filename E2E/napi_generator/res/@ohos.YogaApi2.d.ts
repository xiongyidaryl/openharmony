/*
* Copyright (C) 2022 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import {AsyncCallback, Callback} from "./basic";

/**
 * Provides YOGA APIs
 *
 * @since 8
 * @syscap SystemCapability.YogaApi
 */
declare namespace YOGA {
	/**********YOGA config 相关*************/

	function YogaConfigCreate():YogaConfig;
	function YogaNodeCreate():YogaNode;

	export interface Config {
		Create():Config;
		destroy(config:Config):void;
		setExperimentalFeatureEnabled(feature:number, enabled:boolean):void;
		setPointScaleFactor(pixelsInPoint:number):void;
		isExperimentalFeatureEnabled(feature:number):boolean;
		
	/*	mConfig:YGConfigRef; */
		mConfigPointer:number;
	}

	export interface Value {
		unit:number;
		value:number;
	}
	
	export interface Size {
		width:number;
		height:number;
	}
	export interface Layout {
		top:number;
		bottom:number;
		left:number;
		right:number;
		
		width:number;
		height:number;
	}

	export interface YogaNode {
		createDefault():YogaNode;
		createWithConfig(config:Config):YogaNode;
		destroy(node:YogaNode);
		
		/*fromYGNode(node:YGNodeRef):Node;*/
		reset():void;
		copyStyle(other:Node):void;
		setPositionType(positionType:number):void;
		setPosition(edge:number, position:number):void;
		setPositionPercent(edge:number, percent:number):void;
		setAlignContent(alignContent:number):void;
		setAlignItems(alignItems:number):void;
  
		setAlignSelf(alignSelf:number):void;
		setFlexDirection(flexDirection:number):void;
		setFlexWrap(flexWrap:number):void;
		setJustifyContent(justifyContent:number):void;

		setMargin(edge:number, margin:number):void;
		setMarginPercent(edge:number, margin:number):void;
		setMarginAuto(edge:number):void;
		
		setOverflow(overflow:number):void;
		setDisplay(display:number):void;
		
		setFlex(flex:number):void;
		setFlexBasis(flexBasis:number):void;
		setFlexBasisPercent(flexBasis:number):void;
		setFlexBasisAuto():void;
		setFlexGrow(flexGrow:number):void;
		setFlexShrink(flexShrink:number):void;

		setWidth(width:number):void;
		setWidthPercent(width:number):void;
		setWidthAuto():void;
		setHeight(height:number):void;
		setHeightPercent(height:number):void;
		setHeightAuto():void;

		setMinWidth(minWidth:number):void;
		setMinWidthPercent(minWidth:number):void;
		setMinHeight(minHeight:number):void;
		setMinHeightPercent(minHeight:number):void;
	
		setMaxWidth(maxWidth:number):void;
		setMaxWidthPercent(maxWidth:number):void;
		setMaxHeight(maxHeight:number);
		setMaxHeightPercent(maxHeight:number):void;

		setAspectRatio(aspectRatio:number):void;

		setBorder(edge:number, border:number):void;

		setPadding(edge:number, padding:number):void;
		setPaddingPercent(edge:number, padding:number):void;
		getPositionType():number;
		getPosition(edge:number):Value;

		getAlignContent():number;
		getAlignItems():number;
		getAlignSelf():number;
		getFlexDirection():number;
		getFlexWrap():number;
		getJustifyContent():number;

		getMargin(edge:number):Value;

		getOverflow():number;
		getDisplay():number;

		getFlexBasis():Value;
		getFlexGrow():number;
		getFlexShrink():number;

		getWidth():Value;
		getHeight():Value;
		
		getMinWidth():Value;
		getMinHeight():Value;
		
		getMaxWidth():Value;
		getMaxHeight():Value;
		
		getAspectRatio():number;

		getBorder(edge:number):number;

		getPadding(edge:number):Value;
		insertChild(child:Node, index:number):void;
		removeChild(child:Node):void;
		getChildCount():number;
		getParent():Node;
		getChild(index:number):Node;
		markDirty():void;
		isDirty():boolean;
		calculateLayout(width:number, height:number, direction:number):void;
		getComputedLeft():number;
		getComputedRight():number;

		getComputedTop():number;
		getComputedBottom():number;
		
		getComputedWidth():number;
		getComputedHeight():number;
		
		getComputedLayout():Layout;
		
		getComputedMargin(edge:number):number;
		getComputedBorder(edge:number):number;
		getComputedPadding(edge:number):number;
		setIsReferenceBaseline(isReferenceBaseline:boolean):void;
		isReferenceBaseline():boolean;

	}
}
