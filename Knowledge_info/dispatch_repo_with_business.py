#!/usr/bin/env python
# coding = UTF-8

import openpyxl
import datetime
import os
from gitee_config import OUTPUT_DIRS_DEFAULT


def dispatch_repo_with_business(knowledge_white, tpc_white, repo_file=None, save_file=None):
    business_repos = dict()
    business_repos["knowledge"] = dict()
    business_repos["tpc"] = dict()
    business_repos["others"] = dict()

    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)
    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, "repos_in_business.xlsx")

    if not repo_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        repo_file = os.path.join(work_dir, "repos_in_owner.xlsx")

    # 处理知识体系相关的仓库白名单
    # wb = openpyxl.load_workbook("data/knowledge_repos_white.xlsx")
    wb = openpyxl.load_workbook(knowledge_white)
    sheet = wb.active
    rows = tuple(sheet.rows)[1:]
    for row in rows:
        repo_name = row[0].value
        repo_owner = row[1].value
        if repo_name not in business_repos["knowledge"]:
            business_repos["knowledge"][repo_name] = dict()
            business_repos["knowledge"][repo_name]["owner"] = repo_owner

    # 处理TPC相关的仓库白名单
    # wb = openpyxl.load_workbook("data/tpc_repos_white.xlsx")
    wb = openpyxl.load_workbook(tpc_white)
    sheet = wb.active
    rows = tuple(sheet.rows)[1:]
    for row in rows:
        repo_name = row[0].value
        repo_owner = row[1].value
        if repo_name not in business_repos["tpc"]:
            business_repos["tpc"][repo_name] = dict()
            business_repos["tpc"][repo_name]["owner"] = repo_owner

    wb = openpyxl.load_workbook(repo_file)
    sheet_names = wb.sheetnames
    for sheet_name in sheet_names:
        owner = sheet_name
        sheet = wb[sheet_name]
        rows = tuple(sheet.rows)[1:]
        for row in rows:
            repo_name = row[0].value
            created_time = row[1].value
            if owner == "openharmony-tpc":  # all the tpc repos should go to tpc
                business_repos["tpc"][repo_name] = dict()
                business_repos["tpc"][repo_name]["owner"] = owner
                business_repos["tpc"][repo_name]["created_time"] = created_time
                continue

            if repo_name in business_repos["tpc"]:
                business_repos["tpc"][repo_name]["owner"] = owner
                business_repos["tpc"][repo_name]["created_time"] = created_time
            elif repo_name in business_repos["knowledge"]:
                business_repos["knowledge"][repo_name]["owner"] = owner
                business_repos["knowledge"][repo_name]["created_time"] = created_time
            else:
                business_repos["others"][repo_name] = dict()
                business_repos["others"][repo_name]["owner"] = owner
                business_repos["others"][repo_name]["created_time"] = created_time

    # save the data to the excel
    wb = openpyxl.Workbook()
    wb.remove(wb.active)
    for business_name, repos in business_repos.items():
        sheet = wb.create_sheet(business_name)
        sheet.cell(1, 1, "repo_name")
        sheet.cell(1, 2, "repo_owner")
        sheet.cell(1, 3, "created_time")
        row = 2
        for repo_name, info in repos.items():
            sheet.cell(row, 1, repo_name)
            sheet.cell(row, 2, info["owner"])
            if "created_time" not in info:
                print(repo_name, info)
            sheet.cell(row, 3, info["created_time"])
            row += 1
    wb.save(save_file)


if __name__ == "__main__":
    dispatch_repo_with_business()