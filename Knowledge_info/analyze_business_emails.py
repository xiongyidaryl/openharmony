#!/usr/bin/env python
# coding = UTF-8
import openpyxl
from gitee_config import OUTPUT_DIRS_DEFAULT
import datetime
import os
import copy


def analyze_business_emails(commits_email_file: str = None, save_file: str = None):
    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)
    commit_emails = dict()

    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, "analyze_emails.xlsx")

    if not commits_email_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        commits_email_file = os.path.join(work_dir, "business_emails.xlsx")

    wb = openpyxl.load_workbook(commits_email_file)
    sheet_names = wb.sheetnames
    for sheet_name in sheet_names:
        business_name = sheet_name
        commit_emails[business_name] = dict()
        sheet = wb[sheet_name]
        rows = tuple(sheet.rows)[1:]
        for row in rows:
            email = row[0].value
            author = row[1].value
            email_type = row[2].value
            email_company = row[3].value
            commit_emails[business_name][email] = dict()
            commit_emails[business_name][email]["author"] = author
            commit_emails[business_name][email]["type"] = email_type
            commit_emails[business_name][email]["company"] = email_company

    emails_knowledge = commit_emails["knowledge"]
    emails_tpc = commit_emails["tpc"]
    emails_others = commit_emails['others']
    emails_knowledge_tpc = copy.deepcopy(emails_knowledge)
    for email, info in emails_tpc.items():
        if email in emails_knowledge_tpc:
            emails_knowledge_tpc[email] = dict()
            emails_knowledge_tpc[email]["author"] = info["author"]
            emails_knowledge_tpc[email]["type"] = info["type"]
            emails_knowledge_tpc[email]["company"] = info["company"]

    emails_knowledge_tpc_other_share = dict()
    for email, info in emails_knowledge_tpc.items():
        if email in emails_others:
            emails_knowledge_tpc_other_share[email] = dict()
            emails_knowledge_tpc_other_share[email]["author"] = info["author"]
            emails_knowledge_tpc_other_share[email]["type"] = info["type"]
            emails_knowledge_tpc_other_share[email]["company"] = info["company"]

    wb = openpyxl.Workbook()
    wb.remove(wb.active)

    sheet = wb.create_sheet("knowledge")
    sheet.cell(1, 1, "email")
    sheet.cell(1, 2, "author")
    sheet.cell(1, 3, "type")
    sheet.cell(1, 4, "company")
    row = 2
    for email, info in emails_knowledge.items():
        sheet.cell(row, 1, email)
        sheet.cell(row, 2, info["author"])
        sheet.cell(row, 3, info["type"])
        sheet.cell(row, 4, info["company"])
        row += 1

    sheet = wb.create_sheet("tpc")
    sheet.cell(1, 1, "email")
    sheet.cell(1, 2, "author")
    sheet.cell(1, 3, "type")
    sheet.cell(1, 4, "company")
    row = 2
    for email, info in emails_tpc.items():
        sheet.cell(row, 1, email)
        sheet.cell(row, 2, info["author"])
        sheet.cell(row, 3, info["type"])
        sheet.cell(row, 4, info["company"])
        row += 1

    sheet = wb.create_sheet("others")
    sheet.cell(1, 1, "email")
    sheet.cell(1, 2, "author")
    sheet.cell(1, 3, "type")
    sheet.cell(1, 4, "company")
    row = 2
    for email, info in emails_others.items():
        sheet.cell(row, 1, email)
        sheet.cell(row, 2, info["author"])
        sheet.cell(row, 3, info["type"])
        sheet.cell(row, 4, info["company"])
        row += 1

    sheet = wb.create_sheet("knowledge_tpc")
    sheet.cell(1, 1, "email")
    sheet.cell(1, 2, "author")
    sheet.cell(1, 3, "type")
    sheet.cell(1, 4, "company")
    row = 2
    for email, info in emails_knowledge_tpc.items():
        sheet.cell(row, 1, email)
        sheet.cell(row, 2, info["author"])
        sheet.cell(row, 3, info["type"])
        sheet.cell(row, 4, info["company"])
        row += 1

    sheet = wb.create_sheet("knowledge_tpc_other_share")
    sheet.cell(1, 1, "email")
    sheet.cell(1, 2, "author")
    sheet.cell(1, 3, "type")
    sheet.cell(1, 4, "company")
    row = 2
    for email, info in emails_knowledge_tpc_other_share.items():
        sheet.cell(row, 1, email)
        sheet.cell(row, 2, info["author"])
        sheet.cell(row, 3, info["type"])
        sheet.cell(row, 4, info["company"])
        row += 1

    wb.save(save_file)
    return None


if __name__ == "__main__":
    analyze_business_emails()
