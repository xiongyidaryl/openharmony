from concurrent.futures import ThreadPoolExecutor, wait, ALL_COMPLETED
import threading
import requests
import datetime
import time

from requests.adapters import HTTPAdapter


def __get_time():
    current_time = datetime.datetime.now().strftime('%Y-%m-%d %X')
    return current_time


def time_me(fn):
    def _wrapper(*args, **kwargs):
        start = datetime.datetime.now()	 # 获取当前时间
        ret = fn(*args, **kwargs)
        end = datetime.datetime.now()
        print(f"[{__get_time()}] --->function:{fn.__name__} {args} {kwargs} cost {(end-start).seconds} seconds")
        return ret
    return _wrapper


@time_me
def gitee_get_openapi(url=None, headers=None, params=None):
    requests.adapters.DEFAULT_RETRIES = 10
    s = requests.session()
    s.keep_alive = False
    s.mount('http://', HTTPAdapter(max_retries=10))
    s.mount('https://', HTTPAdapter(max_retries=10))

    try:
        response = s.get(url=url, headers=headers, params=params, timeout=10)
    except requests.exceptions.RequestException as e:
        print(f"spider {url} {headers} {params} Exception")
        return None
    return response


def __action(url: str, header: dict, param: dict, flags: dict):
    response = gitee_get_openapi(url, header, param)
    return flags, response


def spider(max_workers: int, tasks: list):
    result = list()
    futures = list()
    with ThreadPoolExecutor(max_workers=1000) as pool:
        for task in tasks:
            url = task[0]
            header = task[1]
            param = task[2]
            flag = task[3]
            futures.append(pool.submit(__action, url, header, param, flag))
    for future in futures:
        result.append(future.result())
    return result


def __main():
    url = "https://gitee.com/api/v5/orgs/openharmony-sig/repos"
    header = dict()
    header["Content-Type"] = 'application/json'
    header["charset"] = 'UTF-8'
    flag = ["openharmony-sig"]
    tasks = list()
    for page in range(100):
        param = dict()
        param["access_token"] = "1b0038e8549006c6f55fc654cf43f6de"
        param["page"] = page
        param["per_page"] = 1
        param["type"] = "all"
        task = (url, header, param, flag)
        tasks.append(task)
    ret = spider(20, tasks)
    print(ret)


if __name__ == "__main__":
    __main()

