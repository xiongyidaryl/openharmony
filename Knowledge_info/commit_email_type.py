def get_email_type(email="zhangqf@fn-link.com"):
    com_dict = dict()
    com_dict['@allwinnertech.com'] = '全志'
    com_dict['@holdiot.com'] = '小熊派'
    com_dict['@bearpi.cn'] = '小熊派'
    com_dict['@kaihongdigi.com'] = '深开鸿'
    com_dict['@bestechnic.com'] = '恒玄'
    com_dict['@bekencorp.com'] = '博通'
    com_dict['@fn-link.com'] = '欧智通'
    com_dict['@goodix.com'] = '汇顶'
    com_dict['@hoperun.com'] = '润和软件'
    com_dict['@lango-tech.cn'] = '朗国'
    com_dict['@archermind.com '] = '诚迈科技'
    com_dict['@billjc.com'] = '武汉佰钧成'
    com_dict['@chinasoftinc.com'] = '中软国际'
    com_dict['@chinasoft.com'] = '中软国际'
    com_dict['@digitalchina.com'] = '神州数码'
    com_dict['@hyperchain.cn'] = '趣链科技'
    com_dict['@ingenic.com'] = '北京君正'
    com_dict['@iscas.ac.cn'] = '中国科学院软件研究所'
    com_dict['@nj.iscas.ac.cn'] = '中国科学院软件研究所'
    com_dict['@isoftstone.com'] = '软通动力'
    com_dict['@ISS.isoftstone.com'] = '软通动力'
    com_dict['@jd.com'] = '京东'
    com_dict['@thundersoft.com'] = '中科创达'
    com_dict['@talkweb.com.cn'] = '拓维信息'
    com_dict['@nucleisys.com'] = '芯来科技'
    com_dict['@midea.com'] = '美的'
    com_dict['@hiharmonica.com'] = '深鸿会'
    com_dict['@kikatech.com'] = '北京新美互通'
    com_dict['@chipsea.com'] = '芯海科技'
    com_dict['@dt4sw.com'] = '狄泰软件'
    com_dict['@neusoft.com'] = '东软集团'
    com_dict['@superred.com.cn'] = '万里红科技'
    com_dict['@kotei-info.com'] = '武汉光庭信息'
    com_dict['@rock-chips.com'] = '瑞芯微'
    com_dict['@snqu.com'] = '盛趣'
    com_dict['@vyagoo.com'] = '研果科技'
    com_dict['@urovo.com'] = '优博讯'
    com_dict['@tes-tec.com'] = '宸展光电'
    com_dict['@realsil.com.cn'] = '瑞晟'
    com_dict['@xgimi.com'] = '极米'
    com_dict['@geohey.com'] = '极海'
    com_dict['@unionman.com.cn'] = '九联科技'
    com_dict['ecarxgroup.com'] = '亿咖通科技'
    com_dict['@pateo.com.cn'] = '博泰'
    com_dict['@malata.com'] = '万利达'
    # com_dict['@huawei.com'] = '华为'
    email_fmt = "@" + email.split("@")[-1]
    if email_fmt in com_dict:
        email_type = "企业开发者"
    else:
        email_type = "个人开发者"
    return email_type

def get_email_company(email: str = None):
    com_dict = dict()
    com_dict['@allwinnertech.com'] = '全志'
    com_dict['@holdiot.com'] = '小熊派'
    com_dict['@bearpi.cn'] = '小熊派'
    com_dict['@kaihongdigi.com'] = '深开鸿'
    com_dict['@bestechnic.com'] = '恒玄'
    com_dict['@bekencorp.com'] = '博通'
    com_dict['@fn-link.com'] = '欧智通'
    com_dict['@goodix.com'] = '汇顶'
    com_dict['@hoperun.com'] = '润和软件'
    com_dict['@lango-tech.cn'] = '朗国'
    com_dict['@archermind.com '] = '诚迈科技'
    com_dict['@billjc.com'] = '武汉佰钧成'
    com_dict['@chinasoftinc.com'] = '中软国际'
    com_dict['@chinasoft.com'] = '中软国际'
    com_dict['@digitalchina.com'] = '神州数码'
    com_dict['@hyperchain.cn'] = '趣链科技'
    com_dict['@ingenic.com'] = '北京君正'
    com_dict['@iscas.ac.cn'] = '中国科学院软件研究所'
    com_dict['@nj.iscas.ac.cn'] = '中国科学院软件研究所'
    com_dict['@isoftstone.com'] = '软通动力'
    com_dict['@ISS.isoftstone.com'] = '软通动力'
    com_dict['@jd.com'] = '京东'
    com_dict['@thundersoft.com'] = '中科创达'
    com_dict['@talkweb.com.cn'] = '拓维信息'
    com_dict['@nucleisys.com'] = '芯来科技'
    com_dict['@midea.com'] = '美的'
    com_dict['@hiharmonica.com'] = '深鸿会'
    com_dict['@kikatech.com'] = '北京新美互通'
    com_dict['@chipsea.com'] = '芯海科技'
    com_dict['@dt4sw.com'] = '狄泰软件'
    com_dict['@neusoft.com'] = '东软集团'
    com_dict['@superred.com.cn'] = '万里红科技'
    com_dict['@kotei-info.com'] = '武汉光庭信息'
    com_dict['@rock-chips.com'] = '瑞芯微'
    com_dict['@snqu.com'] = '盛趣'
    com_dict['@vyagoo.com'] = '研果科技'
    com_dict['@urovo.com'] = '优博讯'
    com_dict['@tes-tec.com'] = '宸展光电'
    com_dict['@realsil.com.cn'] = '瑞晟'
    com_dict['@xgimi.com'] = '极米'
    com_dict['@geohey.com'] = '极海'
    com_dict['@unionman.com.cn'] = '九联科技'
    com_dict['ecarxgroup.com'] = '亿咖通科技'
    com_dict['@pateo.com.cn'] = '博泰'
    com_dict['@malata.com'] = '万利达'
    # com_dict['@huawei.com'] = '华为'
    email_fmt = "@" + email.split("@")[-1]
    if email_fmt in com_dict:
        company = com_dict[email_fmt]
    else:
        company = "NA"
    return company


def check_email_valid(email: str):
    email_fmt_invalid = ["@huawei.com", "@user.noreply.gitee.com", "@mail.openharmony.io", "@openharmony", "@users.noreply.github.com"]
    email_fmt = "@" + email.split("@")[-1]
    if email_fmt in email_fmt_invalid:
        ret = False
    else:
        ret = True
    return ret


if __name__ == "__main__":
    email_test_cases = ["xxxx@tes-tec.com", "xx@163.com", "xxxxx@huawei.com"]
    for email in email_test_cases:
        if check_email_valid(email):
            print(get_email_type(email))
        else:
            print(f"invalid email {email}")
