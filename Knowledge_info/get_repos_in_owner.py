#!/usr/bin/env python
# coding = UTF-8
import os.path

from spiders import gitee_get_openapi, spider
from gitee_config import GITEE_ACCESS_TOKEN, OUTPUT_DIRS_DEFAULT
import openpyxl
import datetime

ITEM_PER_PAGE = 20


def __get_repos_in_owner(owner="openharmony-sig"):
    url = f"https://gitee.com/api/v5/orgs/{owner}/repos"
    header = dict()
    header["Content-Type"] = 'application/json'
    header["charset"] = 'UTF-8'
    flag = ["openharmony-sig"]
    tasks = list()
    param = dict()
    param["access_token"] = "1b0038e8549006c6f55fc654cf43f6de"
    param["page"] = 1
    param["per_page"] = 1
    param["type"] = "all"
    task = (url, header, param, flag)
    tasks.append(task)
    result = spider(20, tasks)
    flag, response = result[0]
    if response.status_code == 200:
        num = int(response.headers["total_count"])
    else:
        print("ERR:Failed to get the repos num")
        return
    per_page = 10
    pages = int((num + ITEM_PER_PAGE - 1)/ITEM_PER_PAGE)
    tasks.clear()
    for page in range(pages):
        param = dict()
        param["access_token"] = "1b0038e8549006c6f55fc654cf43f6de"
        param["page"] = page + 1
        param["per_page"] = ITEM_PER_PAGE
        param["type"] = "all"
        task = (url, header, param, flag)
        tasks.append(task)

    result = spider(1000, tasks)
    repos = list()
    for flag, response in result:
        if response.status_code == 200:
            repo_items = response.json()
            for repo_item in repo_items:
                repo = repo_item["path"], repo_item["created_at"]
                repos.append(repo)
    return repos


def get_repos_all(save_file=None):
    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)
    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, "repos_in_owner.xlsx")
    owners = ["openharmony-sig", "openharmony", "openharmony-tpc"]
    wb = openpyxl.Workbook()
    wb.remove(wb.active)
    for owner in owners:
        sheet = wb.create_sheet(owner)
        repos = __get_repos_in_owner(owner)
        sheet.cell(1, 1, "repo name")
        sheet.cell(1, 2, "create time")
        row = 2
        for repo in repos:
            sheet.cell(row, 1, repo[0])
            sheet.cell(row, 2, repo[1])
            row += 1
    wb.save(save_file)
    return


if __name__ == "__main__":
    get_repos_all()