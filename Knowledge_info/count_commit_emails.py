#!/usr/bin/env python
# coding = UTF-8
import openpyxl
from gitee_config import OUTPUT_DIRS_DEFAULT
import datetime
import os
from commit_email_type import  get_email_type, get_email_company, check_email_valid


def __save_commits_email(commits_email: str = None, save_file: dict = None):
    wb = openpyxl.Workbook()
    wb.remove(wb.active)
    for business_name, emails_info in commits_email.items():
        sheet = wb.create_sheet(business_name)
        sheet.cell(1, 1, "email")
        sheet.cell(1, 2, "author")
        sheet.cell(1, 3, "type")
        sheet.cell(1, 4, "company")
        row = 2
        for email, info in emails_info.items():
            try:
                sheet.cell(row, 1, email)
                sheet.cell(row, 2, info["author"])
                sheet.cell(row, 3, info["type"])
                sheet.cell(row, 4, info["company"])
            except:
                print(f"save commit information {email} {info} failed")
            row += 1
    wb.save(save_file)


def count_business_emails(commits_info_file: str = None, save_file: str = None):
    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)
    commit_emails = dict()

    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, "business_emails.xlsx")

    if not commits_info_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        commits_info_file = os.path.join(work_dir, "business_commits.xlsx")

    wb = openpyxl.load_workbook(commits_info_file)
    sheet_names = wb.sheetnames
    for sheet_name in sheet_names:
        business_name = sheet_name
        commit_emails[business_name] = dict()
        sheet = wb[sheet_name]
        rows = tuple(sheet.rows)[1:]
        for row in rows:
            author = row[3].value
            email = row[5].value
            if email and check_email_valid(email):
                if email not in commit_emails[business_name]:
                    commit_emails[business_name][email] = dict()
                    commit_emails[business_name][email]["author"] = author
                    commit_emails[business_name][email]["type"] = get_email_type(email)
                    commit_emails[business_name][email]["company"] = get_email_company(email)
    print(commit_emails)
    __save_commits_email(commit_emails, save_file)
    return commit_emails


if __name__ == "__main__":
    count_business_emails()
