#!/usr/bin/env python
# coding = UTF-8
import re

import openpyxl
from concurrent.futures import ThreadPoolExecutor, wait, ALL_COMPLETED

from gitee_config import GITEE_ACCESS_TOKEN, OUTPUT_DIRS_DEFAULT
import datetime
import os
from spiders import gitee_get_openapi

ITEMS_PER_PAGE = 20
REPO_MAX_PAGES = 100000


def __make_repo_commits_task(business_name: str, repo_owner: str, repo_name: str, repo_created_time: str):
    # make the url
    url = f"https://gitee.com/api/v5/repos/{repo_owner}/{repo_name}/commits"

    # make the request header
    header = dict()
    header["Content-Type"] = 'application/json'
    header["charset"] = 'UTF-8'

    # make the request param
    param = dict()
    param["access_token"] = GITEE_ACCESS_TOKEN
    param["page"] = 1
    param["per_page"] = ITEMS_PER_PAGE
    param["since"] = repo_created_time

    # make the flag
    flag = (business_name, repo_owner, repo_name)

    return url, header, param, flag


def __worker_function(task_param: tuple):
    commits_info = list()

    url = task_param[0]
    header = task_param[1]
    param = task_param[2]
    flag = task_param[3]

    business_name = flag[0]
    repo_owner = flag[1]
    repo_name = flag[2]

    for page in range(REPO_MAX_PAGES):
        param["page"] = page + 1
        response = gitee_get_openapi(url, header, param)
        if response and response.status_code == 200:
            commit_items = response.json()
            for commit_item in commit_items:
                commit_info = dict()
                commit_info["repo_name"] = repo_name
                commit_info["repo_owner"] = repo_owner
                commit_info["business_name"] = business_name
                commit_info["author"] = commit_item["commit"]["author"]["name"]
                commit_info["date"] = commit_item["commit"]["author"]["date"]
                # commit_info["email"] = commit_item["commit"]["author"]["email"].replace('\\x1b', '')
                commit_info["email"] = re.compile('[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]').sub(' ', commit_item["commit"]["author"]["email"])
                commit_info["commit_id"] = commit_item["sha"]
                commit_info["commit_url"] = commit_item["html_url"]
                # commit_info["commit_message"] = commit_item["commit"]["message"].replace('\\x08', '')
                commit_info["commit_message"] = re.compile('[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]').sub(' ', commit_item["commit"]["message"])
                commits_info.append(commit_info)

            if len(commit_items) != ITEMS_PER_PAGE:
                break
        else:
            break
    return commits_info


def __save_commits_info(commits_info: dict, save_file: str = None):
    wb = openpyxl.Workbook()
    wb.remove(wb.active)
    for business_name, commits in commits_info.items():
        sheet = wb.create_sheet(business_name)
        sheet.cell(1, 1, "repo_name")
        sheet.cell(1, 2, "repo_owner")
        sheet.cell(1, 3, "business_name")
        sheet.cell(1, 4, "author")
        sheet.cell(1, 5, "date")
        sheet.cell(1, 6, "email")
        sheet.cell(1, 7, "commit_id")
        sheet.cell(1, 8, "commit_url")
        sheet.cell(1, 9, "commit_message")
        row = 2
        for commit_info in commits:
            try:
                sheet.cell(row, 1, commit_info["repo_name"])
                sheet.cell(row, 2, commit_info["repo_owner"])
                sheet.cell(row, 3, commit_info["business_name"])
                sheet.cell(row, 4, commit_info["author"])
                sheet.cell(row, 5, commit_info["date"])
                sheet.cell(row, 6, commit_info["email"])
                sheet.cell(row, 7, commit_info["commit_id"])
                sheet.cell(row, 8, commit_info["commit_url"])
                sheet.cell(row, 9, commit_info["commit_message"])
            except:
                print(f"save commit information {commit_info} failed")
                pass
            row += 1
    wb.save(save_file)
    return None


def get_all_business_commits(business_repo_file: str = None, save_file: str = None):
    task_params = list()
    time_now = datetime.datetime.now()
    date_time = "%s-%s-%s" % (time_now.year, time_now.month, time_now.day)

    if not save_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        save_file = os.path.join(work_dir, "business_commits.xlsx")

    if not business_repo_file:
        if not (os.path.exists(OUTPUT_DIRS_DEFAULT)):
            os.mkdir(OUTPUT_DIRS_DEFAULT)
        work_dir = os.path.join(OUTPUT_DIRS_DEFAULT, date_time)
        if not (os.path.exists(work_dir)):
            os.mkdir(work_dir)
        business_repo_file = os.path.join(work_dir, "repos_in_business.xlsx")

    wb = openpyxl.load_workbook(business_repo_file)
    sheet_names = wb.sheetnames
    for sheet_name in sheet_names:
        sheet = wb[sheet_name]
        rows = tuple(sheet.rows)[1:]
        for row in rows:
            business_name = sheet_name
            repo_name = row[0].value
            repo_owner = row[1].value
            repo_created_time = row[2].value
            task_param = __make_repo_commits_task(business_name, repo_owner, repo_name, repo_created_time)
            task_params.append(task_param)

    commits_info = dict()
    futures = list()
    with ThreadPoolExecutor(max_workers=20) as pool:
        for task_param in task_params:
            futures.append(pool.submit(__worker_function, task_param))
    for future in futures:
        # result.append(future.result())
        repo_commits_info = future.result()
        for commit_info in repo_commits_info:
            business_name = commit_info["business_name"]
            if business_name not in commits_info:
                commits_info[business_name] = list()
            commits_info[business_name].append(commit_info)
    print(commits_info)
    __save_commits_info(commits_info, save_file)
    return None


if __name__ == "__main__":
    # gitee_get_commits(commits_num_file="out/2022-5-16/gitee_commits_num.xlsx")
    get_all_business_commits()