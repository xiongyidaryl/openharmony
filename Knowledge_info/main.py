# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os
import sys

from PyQt5 import uic
from PyQt5.QtCore import Qt, pyqtSignal, QThread
from PyQt5.QtWidgets import QApplication, QWidget, QFileDialog

from get_repos_in_owner import get_repos_all
from dispatch_repo_with_business import dispatch_repo_with_business
from get_business_commits import get_all_business_commits
from count_commit_emails import count_business_emails
from analyze_business_emails import analyze_business_emails

#实现多线程，避免GUI界面卡顿
class MyThread(QThread):
    thread_signal = pyqtSignal(str)
    def __init__(self):
        super().__init__()
        self.knowledge_input = ''
        self.tpc_input = ''

    def run(self):
        msg = ''
        if (self.knowledge_input.endswith('.xlsx')
           and self.tpc_input.endswith('.xlsx')):
            self.thread_signal.emit(msg+'【导入数据表成功】')
            self.thread_signal.emit(msg+'【Begin to update the repos】')
            get_repos_all()
            self.thread_signal.emit(msg + '【Output file successfully: repos_in_owner.xlsx 】')
            self.thread_signal.emit(msg + '【Begin to dispatch the repos to business】')
            dispatch_repo_with_business(self.knowledge_input, self.tpc_input)
            self.thread_signal.emit(msg + '【Output file successfully: repos_in_business.xlsx 】')
            self.thread_signal.emit(msg + '【Begin to get the  commits of repos in business 】')
            self.thread_signal.emit(msg + '【Please wait patiently...This step will take few minutes... 】')
            get_all_business_commits()
            self.thread_signal.emit(msg + '【Output file successfully: business_commits.xlsx 】')
            self.thread_signal.emit(msg + '【Begin to count the emails in business 】')
            count_business_emails()
            self.thread_signal.emit(msg + '【Output file successfully: business_emails.xlsx 】')
            self.thread_signal.emit(msg + '【Begin to analyze the emails in business 】')
            analyze_business_emails()
            self.thread_signal.emit(msg + '【Output file successfully: analyze_emails.xlsx 】')
            self.thread_signal.emit(msg + '【*******************Done******************************* 】')

class MyWindow(QWidget):
    my_signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.init_ui()
        self.cwd = os.getcwd()
        self.msg_history = list()

    def init_ui(self):
        self.ui = uic.loadUi("./ui/knowledge.ui")
        self.msg=self.ui.label_3
        self.msg.setWordWrap(True)
        self.msg.setAlignment(Qt.AlignTop)

        #提取UI控件
        self.knowledge_input_qwidget=self.ui.lineEdit
        self.tpc_input_qwidget = self.ui.lineEdit_2
        self.knowledge_upload_btn=self.ui.pushButton
        self.tpc_upload_btn = self.ui.pushButton_2
        self.run_btn=self.ui.commandLinkButton
        self.scroll_qwidget=self.ui.scrollArea
        #绑定信号与槽函数
        self.knowledge_upload_btn.clicked.connect(self.upload_knowledge)
        self.tpc_upload_btn.clicked.connect(self.upload_tpc)
        self.run_btn.clicked.connect(self.start_thread)


    def upload_knowledge(self):
        file_path, filetype = QFileDialog.getOpenFileName(self, "选取文件", self.cwd,
                                                                "Text Files (*.xlsx);;All Files (*)")
        if file_path == "":
            return

        self.knowledge_input_qwidget.setText(file_path)

    def upload_tpc(self):
        file_path, filetype = QFileDialog.getOpenFileName(self, "选取文件", self.cwd,
                                                                "Text Files (*.xlsx);;All Files (*)")
        if file_path == "":
            return

        self.tpc_input_qwidget.setText(file_path)

    def start_thread(self):
        self.my_thread = MyThread()  # 创建线程
        self.my_thread.knowledge_input=self.knowledge_input_qwidget.text()
        self.my_thread.tpc_input=self.tpc_input_qwidget.text()
        self.my_thread.thread_signal.connect(self.terminal_show)
        self.my_thread.start()  # 开始线程

    def terminal_show(self, msg):
        print(msg)
        self.msg_history.append(msg)
        self.msg.setText("<br>".join(self.msg_history))
        self.msg.resize(681, self.msg.frameSize().height() + 61)
        self.msg.repaint()

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWindow()
    w.ui.show()
    sys.exit(app.exec_())

